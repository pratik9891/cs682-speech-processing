function [ euclidDistance ] = euclid2vector( vector1, vector2 )
%EUCLID2VECTOR faster implementation using vectors!
    euclidDistance = vector1 - vector2;
    euclidDistance = euclidDistance' * euclidDistance;

end

