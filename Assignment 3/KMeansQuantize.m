function [ minDistMean, minDistances, minCodeword ] = KMeansQuantize(codebook, TestVectors)
    % Takes input a codebook and testVectors
    % Returns 
    % minDistances = minimum distortion of each test vector with the
    % codebook
    % minDistMean = mean of all the distortions 
    % minCodeword = ClusterID of cluster the test vector was assigned to.

    observations = size(TestVectors, 2);
    minDistances = zeros(1, observations);
    minCodeword = zeros(1, observations);
  
    for i = 1:observations
        [minDistances(i), minCodeword(i)] = mindist(TestVectors(:,i),codebook);
    end
    
    minDistMean = mean(minDistances);  
     
end

