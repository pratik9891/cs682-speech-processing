function [ features ] = gencep( signal, Fs, length_ms, advance_ms, Ncep, Delta )
%GENCEP returns feature vectors for given audio signal


samples_N = size(signal,1); %number of input samples
length_s = length_ms/1000; %length in seconds
advance_s = advance_ms/1000;    %advance in seconds
frame_length = floor(length_s*Fs);  %frame length in number of frames
advance_length = floor(advance_s*Fs); % advance length in number of frames

frames_N = floor(samples_N/advance_length); %number of frames
if rem(samples_N,advance_length) >= 0   %dont include non-full frames if any
    frames_N = frames_N - 1;
end

cepstrals = zeros(Ncep,frames_N);%initialize to speed up.

H = hamming(frame_length); %calculate before hand, its always same.

for i = 1:frames_N
    start = (i-1)*advance_length + 1;
    stop = start+frame_length - 1;
    current_frame_samples = signal(start:stop);
    windowed_frame = H .* current_frame_samples;
    W_F = fft(windowed_frame);
    W_F = fftshift(W_F); % can be omitted if values from nyquist to end are taken
    W_F = W_F(1:ceil(frame_length/2));%values till nyquist rate
    WFMagSq = W_F .* conj(W_F);
    WFMagSq = WFMagSq + 1;  % to avoid log0
    WFMagDB = 10*log10(WFMagSq);
    cepstrals(:,i) = dct(WFMagDB, Ncep);
end

if Delta
    features = zeros(Ncep, 2* frames_N);
    deltas = deltaCoefficients(cepstrals);
    features = [cepstrals, deltas];

else
    features = cepstrals(2:end,:);
end

end

function [deltas] = deltaCoefficients(cepstrals)
%DELTACOEFFICIENTS returns delta coefficients for fiven cepstral
%coefficients

    deltas = zeros(size(cepstrals));
    n = length(cepstrals);
    for i = 3:(n-2)
        %use bfxfun as it vecotrizes the subtraction
        deltas(:,i) = bsxfun(@minus, cepstrals(:,i-2), cepstrals(:,i+2));
    end
    deltas(:,1) = bsxfun(@minus, cepstrals(:,1),cepstrals(:,2));%first 
    deltas(:,2) = bsxfun(@minus, cepstrals(:,1),cepstrals(:,3));%second
    deltas(:,n-1) = bsxfun(@minus, cepstrals(:,n),cepstrals(:,n-1));%(n-1)th
    deltas(:,n) = bsxfun(@minus, cepstrals(:,n),cepstrals(:,n-1));%(n)th
    
end




