[x,Fs] = wavread('alfalfa.wav');
F = gencep(x,Fs,20,10,25,false);
imagesc(F);
set(gca,'YDir', 'Normal');
xlabel('Cepstral Coefficients');
ylabel('Number of Coefficients');
