function [ r,Nr ] = frequencies( c )
    counts = c.get_counts();
    r = (1:max(counts))';
    Nr = histc(counts,r);
end


   