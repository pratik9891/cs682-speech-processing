classdef counts
% counts
% Matlab class for storing N-gram counts.
% Author:  Marie A. Roch, 
%          Dept. of Computer Science
%          San Diego State University

    properties
        order = NaN;    % order of N-gram
        map = [];       % hashmap for storing N-gram
    end
    
    methods
        % constructor
        function obj = counts(order, filename)
            % obj = counts(order, filename)
            % Create a new counts object of the specified order.
            % Order controls the N-gram length for which counts will be
            % created.  filename is the file that will be read.

            import java.util.HashMap

            inh = fopen(filename, 'rb');
            if inh == -1, 
                error('Unable to open %s', filename); 
            end

            obj.order = order;
            obj.map = HashMap();  % note that unlike Java, we do not use new

            % process each line
            line = fgetl(inh);  % priming read
            
            everyN = 100;  % show progress everyN lines
            n_line = 1;
            fprintf('line %d: %s\n', n_line, line);
            while line ~= -1    % while not end of file
                rest = ['<s> ', line, ' </s>'];  % add sentence start/end
                history = cell(order,1);  % circular buffer, keep order words
                n_word = -1;  % prime loop
                [word, rest] = strtok(rest);  % pull out next word
                % As long as there are more words in the sentence to process...
                while ~ isempty(word)
                    n_word = n_word + 1;
                    % process the current word
                    hist_idx = mod(n_word, order) + 1;  % index in circular buffer
                    history{hist_idx} = word;  % store in circular buffer
                    
                    if n_word >= order-1  % do we have enough context?
                        % compute previous indices, e.g. if order = 3
                        % and the current word will be stored in history{2}, then
                        % we want previous_idx = [3 1] as history{1} is the
                        % previous word and history{3} is the word before that.
                        previous_idx = rem(hist_idx + [0:order-2], order) + 1;
                        
                        % construct/access the context for this word
                        context = obj.map;  % create new handle to count object
                        for idx = previous_idx
                            % access the hash table associated with this
                            next_context = context.get(history{idx});
                            if isempty(next_context)
                                % create table for this one
                                next_context = HashMap();
                                context.put(history{idx}, next_context);
                            end
                            context = next_context;
                        end
                        % update count
                        if context.containsKey(word)
                            % we've seen this word before, add one
                            context.put(word, context.get(word) + 1);
                        else
                            context.put(word, 1);  % first count
                        end
                    end
                    % retrieve the next word in the line
                    [word, rest] = strtok(rest);
                end
                % Reassure the user we're actually doing something...
                if rem(n_line, everyN) == 0
                    fprintf('line %d: %s\n', n_line, line);
                end
                line = fgetl(inh);
                n_line = n_line+1;
            end  
        end
        
        % Note, While not required for the call, Matlab methods
        % do not hide that the object is a parameter to a function
        % call (Python handles objects similarly).  Using obj serves the
        % same purpose as this in Java or C++.
        function count = get_count(obj, varargin)
            % count = get_count(obj, 'word1', 'word2', ..., 'wordN')
            % Determine number of times words 1 to N have been
            % seen in the training text.
            if length(varargin) ~= obj.order
                error('N-gram %d elements, but order is %d', ...
                    length(varargin), obj.order);
            end
            d = obj.map;
            for idx=1:length(varargin)-1
                d = d.get(varargin{idx});
            end
            count = d.get(varargin{end});
        end
        
        function count_vec = get_counts(obj, varargin)
            % counts = get_counts(obj, 'word1', 'word2', ..., 'wordN-1')
            % Find the counts associated with a context of N-1 words.
            % Currently, we only support N-1 words although in theory
            % we could iterate over the entries of smaller contexts
            % to come up with lower order counts.
            if length(varargin) + 1 ~= obj.order
                error('%d words, must be N-gram order - 1 (%d-1=%d)', ...
                    length(varargin), obj.order, obj.order-1);
            end
            d = obj.map;
            for idx=1:length(varargin)
                d = d.get(varargin{idx});
            end
            count_vec = counts.mapValuesToVector(d);
        end
        
    end
    
    methods (Static, Access=private)
        function vector = mapValuesToVector(map)
            % vector = mapValuesToVector(map)
            % private function to convert the values of a Java hashmap
            % to a vector.  This is necessary as Matlab functions such
            % as max, min, sum, etc. do not operate on Java collections.
            % We assume the values are numeric.
            
            % Retrieve the values of the hashmap and preallocate
            % a Matlab vector for them.
            collection = map.values(); 
            N = collection.size();  % # items
            vector = zeros(N, 1);   % preallocate
            
            % iterate over items, adding them to the vector
            iter = collection.iterator();
            idx = 1;
            while iter.hasNext()
                value = iter.next();
                vector(idx) = value;
                idx = idx + 1;
            end            
        end
    end
                
end