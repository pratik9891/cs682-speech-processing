c = counts(1,'train.txt')
[r, Nr] = frequencies(c);
figure('Name','Nr vs. r');
bar(r,Nr,'histc');
xlabel('r');
ylabel('Nr');
[rs,ns,p] = sgtsmooth(r, Nr, true);