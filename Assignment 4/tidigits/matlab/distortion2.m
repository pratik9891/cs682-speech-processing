function distances = distortion2(Data, CodeWords)
% distances = distortion(Data, CodeWords)
% Compute the pairwise Euclidean distortions between a 
% set of D column vectors (Data), and a set of C codewords
% (C column vectors of the same length).  
%
% distances(c,d) represents the Euclidean distortion
% between the c'th codeword and the d'th data vector.

% Although not necessary for correctness, we will preallocate the
% distortion matrix by creating a matrix of zeros.  This is faster than
% building the matrix incrementally.  The following line *can* be
% deleted, the program will just run slower.
distances = zeros(size(CodeWords, 2), size(Data, 2));

[Dim, TrainingN] = size(Data);
Duplicate = ones(1, TrainingN); % Row vector of ones for Tony's trick

for cwidx = 1:size(CodeWords, 2)
  offset = Data - CodeWords(:, cwidx * Duplicate);
  distances(cwidx, :) = sum(offset .^ 2);
end
