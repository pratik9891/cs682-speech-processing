function Codebook = VQ_InitCodebook(TrainingVecs, NCodeWords)
% Codebook = VQ_InitCodebook(TrainingVecs, N)
% Given a set of column vectors as training data, draw an initial codebook
% of NCodeWords.

% Pick NCodeWords at random
% We could do this with rand, but we would need to make sure that
% we do not pick duplicates.  Reading the documentation for rand,
% we see that there exists a randperm which produces a random 
% permutation of numbers.  Much easier.
RandIndices = randperm(size(TrainingVecs, 2));
InitCodeWordIndices = RandIndices(1:NCodeWords);

% Pick the first N codewords as initial guess.
Codebook = TrainingVecs(:, InitCodeWordIndices);


% Alternatively, we could have taken the first 1 to k data as codewords
% For correlated data like speech, this would have been a poor choice...
NotQuiteAsGood = 0; % Shut code off - we don't want to execute this
if NotQuiteAsGood
  Codebook = TrainingVecs(1:NCodeWords, :);
end





