function [LogLikelihood, OptPath] = hmmViterbi(Model, Data, Cache)
% [LogLikelihood, OptimalStateSequence] = hmmViterbi(Model, Data, Cache)
% Given a model and an observation sequence of row-oriented Data, finds the
% best possible path through the model and the natural log likelihood of the
% that path occurring.
%
% Optional arguments:
% Cache - Contains a structure of precomputed probabilites.  
%
% See also:  hmmProbCache
%
% This code is copyrighted 1997-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,3,nargin));

if nargin > 2
else
  Cache = hmmProbCache(Model, Data, 'Mixtures', 0);
end

Cache.b(Cache.b == 0) = eps;   % avoid log(0)

if nargout > 1
  [LogLikelihood, OptPath] = hmmViterbiAux(Model, Cache.b);
else
  LogLikelihood = hmmViterbiAux(Model, Cache.b);
end
