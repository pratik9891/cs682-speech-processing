function [Model, ProbVector] = ...
    hmmBaumWelchIteration(Model, TrainingData, varargin)
% [Model ProbVector] = hmmBaumWelchIteration(Model, TrainingData, OptionalArgs)
%
% Performs iterative Baum-Welch reestimation for a Hidden Markov Model given
% a set of training data.  
%
% If the user would like to perform some operation (i.e. save the model)
% every N iterations, she may do so by specifying N in the Info structure of
% the model.  Set Model.Info.CheckpointIterations = N and provide a callback
% function in Model.Info.CheckpointFunction.  The checkpoint function must
% be a format string for a function with two arguments, i.e.
%	Model.Info.CheckpointFunction = 'myModelSave(%s, %d);'
%	where argument 1 is the model and argument 2 is the iteration.
%	The function should return the entire model structure which
%	was passed into it along with any possible modificaitons.
%
% Optional args
% Verbosity - Level of output
%       0 - quiet
%       1 - final likelihoods
%       2 - iteration level likelihoods
%
% This code is copyrighted 1997-2005 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,Inf,nargin));

Verbosity = 0;
k = 1;
while k < length(varargin)
  switch varargin{k}
   case 'Verbosity'
    Verbosity = varargin{k+1};
    k=k+2;
   otherwise
    error('Unknown argument "%s"', varargin{k});
  end
end

% The Mex version of the EM algorithm doesn't expect to 
% have the mixture probabilities cached, the Matlab version
% does.

% EMImplementation = input('Matlab or C code (m/c)? ', 's');
EMImplementation = 'c';

if EMImplementation == 'm'
  MixtureRequirements = {'Mixtures', 1};	% Cache mixtures
else
  MixtureRequirements = {'Mixtures', 0};	% Don't cache mixtures
end

% precompute probabilities
Cache = hmmProbCache(Model, TrainingData, MixtureRequirements{:});

% Iteration will continue as long as the probability of estimating the
% training data improves by at least the following percentage AND the
% maximum number of iterations is not exceeded.
ImprovementRatioThreshold = .10;

if isfield(Model.Info, 'MaximumIterations')
  IterationsMax = Model.Info.MaximumIterations;
else
  % Limit to some very expensive number of iterations which
  % would probably result in severe overtraining.
  IterationsMax = 1000;
end


ImprovementThreshold = ImprovementRatioThreshold;
% was log(1+ImprovementRatioThreshold) when using / for threshold,
% but now we use - as these are logs

[Alpha, AlphaScale] = hmmAlpha(Model, TrainingData, Cache);
[LogP, GM] = hmmAlphaProb(AlphaScale);

j = Model.Info.Iterations;
if j > 0
  % Previous iterations
  PreviousLogP = Model.Info.Convergence.Log(j);
  ImprovementRatio = LogP - PreviousLogP;
else
  PreviousLogP = LogP;
  ImprovementRatio = Inf;	% Unknown, allows entry into loop
  % Create vectors which will hold the probability of observing the training
  % sequence after each iteration.  Useful for tracking how iteration 
  % converges.
  HistPreallocSize = min([IterationsMax, 20]);
  Model.Info.Convergence.Log = NaN;
  Model.Info.Convergence.Log = ...
      Model.Info.Convergence.Log(ones(HistPreallocSize,1));
  Model.Info.Convergence.GeomMean = Model.Info.Convergence.Log;
end

while ImprovementRatio >= ImprovementThreshold & j < IterationsMax
  j=j+1;
  Model.Info.Iterations = j;
  Model.Info.Convergence.Log(j) = LogP;
  Model.Info.Convergence.GeomMean(j) = GM;

  if Verbosity >= 2
    fprintf('iteration=%d, Alpha Log Prob %f, ratio %f\n', j, ...
            LogP, ImprovementRatio);
  end
  Beta = hmmBeta(Model, AlphaScale, TrainingData, Cache);
  
  if EMImplementation == 'm'
    Model = hmmBaumWelchGaussMix(Model, Alpha, Beta, TrainingData, ...
				 Cache);
  else
    % Perform one expectation maximization step
    NewModel = hmmEM(TrainingData', Alpha, Beta, Model);
    
    % check for poorly conditioned covariance matrices
    % For double precision, ill conditioned when < following value
    % see _Numerical Recipes in C_, Press et al., 2nd ed., 
    % Cambridge Univ. Press, Cambridge, UK, 1992, p. 61
    IllConditioned = 10e-12;
    CondInv = zeros(hmmModelStates(Model), hmmModelMixtures(Model));
    for s=1:hmmModelStates(Model)
      for m=1:hmmModelMixtures(Model)
        if Model.Mix.cov.Diagonal
          % Diagonal, variances are eigen values
          % Compute reciprocal condition number 
          CondInv(s,m) = ...
              min(diag(NewModel.Mix.cov.Sigma{m,s})) / ...
              max(diag(NewModel.Mix.cov.Sigma{m,s}));
        else
          % estimate reciprocal condition number 
          CondInv(s,m) = 1 ./ condest(NewModel.Mix.cov.Sigma{m,s});
        end
      end
    end

    BadMixtures = find(CondInv < IllConditioned);
    if ~ isempty(BadMixtures)
      % At least one mixture should not be continued.
      % At some point, we might want to consider letting
      % the others proceed, but for now we just stop
      % iteration.
      Labels = [];
      for k=1:length(BadMixtures)
        Labels = [Labels, sprintf('(%d->%f)', ...
                                  BadMixtures(k), CondInv(k))];
      end
      j = j - 1;  % don't count current iteration
      if Verbosity
        fprintf(['HMM:ILL_CONDITIONED_MATRIX', ...
              'stopping after %d iterations (mixture->rcond):  %s'], ...
                j, Labels)
      end
      ImprovementRatio = 0;
      continue
    end
      
    Model= NewModel;

    % limit variances if applicable
    if (Model.Mix.limit > 0)
      % Make sure that variances do not exceed limits
      Model.Mix.cov.Sigma = hmmVarianceLimit(Model);
    end
    
    % Update precomputed values (i.e. determinants)
    Model = hmmPreCompute(Model);
    
  end
  
  Cache = hmmProbCache(Model, TrainingData, MixtureRequirements{:});
  
  [Alpha, AlphaScale] = hmmAlpha(Model, TrainingData, Cache);
  [LogP GM] = hmmAlphaProb(AlphaScale);
  
  ImprovementRatio = LogP - PreviousLogP;
  PreviousLogP = LogP;
  
  % Check for speech corpus specific things to do
  if isfield(Model.Info, 'CheckpointIterations')
    % User has requested checkpoint of model every CheckpointIterations.
    if mod(j, Model.Info.CheckpointIterations) == 0
      % User is expected to provide a function which performs some
      % operation (such as saving the model) when passed the model
      % and the current iteration.  User must provide string and
      % numeric argument fields in function which will be replaced 
      % with the proper arguments, i.e.
      %	Model.Info.CheckpointFunction = 'myCorpusSave(%s, %d);'
      % where argument 1 is the model and argument 2 is the iteration.
      eval(['Model = ', sprintf(Model.Info.CheckpointFunction, 'Model', j)]);
    end
  end
end

if j < IterationsMax & isempty(BadMixtures)
  fprintf(['Iteration stopped after %d iterations ', ...
           'due to threshold (Maximum was %d)'], j, IterationsMax);
end

if Verbosity
  fprintf('Final Alpha Log Prob %f, ratio %f\n', LogP, ImprovementRatio);
end

