/*
 * hmmException.c
 *
 * This code is copyrighted 2003 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#include <mex.h>
#include <matrix.h>

#ifdef UNIX
#include <dlfcn.h>	/* dynamically loaded functions */
#endif

void soThrowWarning(void *soLib, char *Message) {
  if (soLib)
    dlclose(soLib);	/* close lib if needed */

  mexWarnMsgTxt(Message);
}

void soThrowException(void *soLib, char *Message) {
  if (soLib)
    dlclose(soLib);	/* close lib if needed */

  mexErrMsgTxt(Message);
}

  
char * soExceptionCheck() {
  /* Check for errors */
#ifdef UNIX
  return dlerror();
#else
  return NULL;
#endif
}

void soThrowIfException(void *soLib) {
  char	*error;

  error = soExceptionCheck();
  if (error)
    soThrowException(soLib, error);
}


