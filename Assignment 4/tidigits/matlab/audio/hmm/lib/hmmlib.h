/*
 * hmmlib.h
 * HMM library macros and data structures
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#ifndef HMMLIB_H

#define HMMLIB_H	1

#include "ArrayMajor.h"		/* Macros for accessing arrays in
				 * column- or row- major order.
				 */

/* Where should malloc/calloc/free routines come from? */
typedef enum {
  ALLOC_MATLAB = 0,	/* Use Matlab allocaction/deallocation */
  ALLOC_CLIB,		/* Use C library (system) allocation/deallocation */
  ALLOC_RETRIEVE,	/* Access cached version if possible, otherwise
			 * use ALLOC_MATLAB
			 */
} ALLOC_METHOD;

typedef enum {
  pdf_Point = 0,		/* Point proabability */
  pdf_ObsCubature = 1,		/* Cubature integration in observation space */
  pdf_ClosedForm = 2,		/* Uses fundamental theorem of calculus
				 * to find area under curve.  Assumes
				 * observations independent.
				 */
} pdfMethod;

/* Constants */
static const double	MinIEEE = 2.225073858507201e-308;   /* Min double */
#define	MAXCOMPONENTS	100	/* no more than N components/observation */
#define MAXSTRING	256	/* maximum string length */

#define LENGTH(ARRAY, TYPE)	(sizeof(ARRAY) / sizeof(TYPE))
  
typedef struct {

  /* Inverted Variance/Covariance matrices.
   * Only the diagonals (variances) are stored.
   * SigmaInv[m][s] contains a double pointer to a vector of variances
   * for the mix m, state s.
   */
  double	***SigmaInvPtr;

  /* The following structures are pointers to arrays of Matlab doubles.
   * Matlab stores arrays in column-major order.  As these are for arbitrary
   * sizes, we must perform the item computataions manually.  
   * the notation or use 
   */
  const double	*MeansPtr;
} Mixture;

/* Model structure in C.  Note that parts of the model which are
 * not used in the library routines may not be present.
 */
typedef struct {
  ALLOC_METHOD	AllocationType;	/* Allocation done via Matlab/C lib? */
  
  int		States;		/* # state, mixtures, components */
  int		Mixtures;
  int		Components;
  double	* PiPtr;		/* (1:States) */
  double	* APtr;			/* (1:States, 1:States) */
  double	** SigmaInv;		/* Var^{-1} 1:Mixtures,1:States */
  double	** Sigma;		/* Var 1:Mixtures, 1:States */
  double	* MeansPtr;		/* (1:Mixtures,1:Components,1:States)*/
  double	* MixtureWtsPtr;	/* (1:Mixtures, 1:States) */
  double	* pdf_kPtr;		/* (1:Mixtures, 1:States) */
  double	* pdf_logkPtr;		/* (1:Mixtures, 1:States) */
} HMM;


/* Used to cache HMMs across Mex invocations */
typedef struct {
  int	N;
  HMM	**HMM;
} HMMCache;

typedef struct {
  int	BasisCount;
  int	Components;
  const double * Basis;		/* (1:Dimensionality, 1:BasisCount) */
} BasisSet;

/* prototypes */

double hmm_f_Gaussian_ind(const double *, const double *, const double *,
		      const double *, int, int);

double hmm_f_state(const int, HMM *, double *, const double *);

double hmm_f_state_mixture_list(const int, HMM *, double *, const double *,
				int, const double *);

#include "hmmCubature.h"
void hmmCubatureInit(CubatureInfo *, const BasisSet *, 
		     const double *, int);
double hmmCubature(const int State, HMM *, const BasisSet *,
		   const CubatureInfo *, const double *Displacement,
		   const double *ObservationPtr, double *f_Mixtures);

double hmmFundThmCalc(const int State, HMM *, const double *Displacement,
		      const double *ObservationPtr, double *f_Mixtures);

double hmmViterbi(HMM *, double *StatePrPtr, int T, double *OptStateSeqPtr);

void hmmXi(double *Xi, double *Alpha, double *Beta, 
	   HMM *Model, double *Obs, int T);

#endif
