/*
 * hmmModelAccessor.h
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

/* hmmModelAccessorPrototye declared separately due to
 * reliance on Matlab mx primitives.  hmmlib.h is to be
 * be independent of Matlab
 */
HMMCache * hmmCacheAccessor(mxArray const *MxAnyHMM);
int hmmModelPersistentIndexAccessor(mxArray const *MxHMM);
void hmmModelAccessorCached(HMM *DestHMM, HMMCache *Cache, int Index);
void hmmModelAccessor(mxArray const *MxSourceHMM, HMM *DestHMM, ALLOC_METHOD);
void hmmFreeModelAccessor(HMM *, ALLOC_METHOD);
