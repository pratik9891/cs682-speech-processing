/*
 * hmm_f_Gaussian.c
 * pdfs of multivariate Gaussians.
 *
 * This code is copyrighted 1997-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */
#include <math.h>

/*
 * hmm_f_Gaussian_ind
 * Evaluate the pdf of a normal distribution with independent components.
 *
 * Arguments:
 * k = 2 pi^{-n/2} det(Sigma)^{-1/2}
 * Mu = base address of mean vector
 * SigmaInv = inverted covariance matrix (diagonal)
 * x = vector to be evaluated f(x)
 * n = # components in x
 * MuStep - mean vector may not be contiguous.  MuStep indicates
 *	how many values to skip between each entry in the mean vector.
 */

double
hmm_f_Gaussian_ind(const double *k,
		   const double *Mu,
		   const double *SigmaInv,
		   const double *x,
		   int n,
		   int MuStep)
{
  int		c;		/* component index */
  double	f_x = 0.0;	/* value of pdf */
  double	Offset;		/* Mu - x */
  double	Exponential = 0.0;

  /* compute eponential arg:  -.5 (x-Mu)' Sigma^{-1} (x-Mu) */
  for (c=0; c < n; c++) {
    Offset = *x - *Mu;
    Exponential += Offset * Offset * *SigmaInv;
    Mu += MuStep;
    SigmaInv++;
    x++;
  }

  /* Evaluate probability density function */
  f_x = *k * exp(-0.5 * Exponential);
  return f_x;
}

/*
 * logf_Gaussian_ind
 * Evaluate the log pdf of a normal distribution with independent components.
 *
 * Arguments are identical to hmm_f_Gaussian_ind with the exception that
 * the the logarithm of k is passed in.  As this can be precomputed
 * when the density is estimated, this results in a slight optimization
 * (the exponentional function call is eliminated).
 */

double
hmm_logf_Gaussian_ind(const double *logk,
		   const double *Mu,
		   const double *SigmaInv,
		   const double *x,
		   int n,
		   int MuStep)
{
  int		c;		/* component index */
  double	f_x = 0.0;	/* value of pdf */
  double	Offset;		/* Mu - x */
  double	logExponential = 0.0;

  /* compute eponential arg:  -.5 (x-Mu)' Sigma^{-1} (x-Mu) */
  for (c=0; c < n; c++) {
    Offset = *x - *Mu;
    logExponential += Offset * Offset * *SigmaInv;
    Mu += MuStep;
    SigmaInv++;
    x++;
  }

  /* Evaluate probability density function */
  f_x = *logk + (-0.5 * logExponential);
  return f_x;
}
