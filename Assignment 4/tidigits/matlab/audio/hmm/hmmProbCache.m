function Cache = hmmProbCache(Model, Data, varargin)
% Cache = hmmProbCache(Model, Data, OptArgs)
% Creates a cache of the probabilities of observing data with
% respect to a certain model and data sequence.  
%
% The optional arguments are as keyword/value pairs as follows:
%	'Mixtures', N - if nonzero, probabilities of mixtures should
%		be cached as well.  This is the default.
%	'Method', MethodStructure
%		When present, directs a type of processing to be
%		performed, as directed by the Type field of the
%		MethodStructure.  Valid Type fields:
%
%		'none' - Standard probability computation.
%		'closed_form' - Integration via the fundamental theorem
%			of calculus.
%		'cubature' - Integration via a cubature method.
%			When cubature is specified, the substructure
%			Cubature must be present.
%			Valid Cubature fields:
%				Panels - Number of panels to approximate
%					the integration.
%				PrincipalComponents - Use principal components
%					to reduce the dimensionality
%					of the problem.  The following
%					subfields are supported:
%					Components - Number of components
%					Method 'varcovar' | 'correlation' -
%						Specifies whether to use
%						variance-covariance or
%						autocorrelation for
%						determining the eigenvectors.
%		Other sub-fields of the MethodStructure:
%		Region - Required for any technique which operates over
%			a region such as an integral or minimax method.
%			Region.Type indicates the region type:
%
%			'ErrorModel' - Statistical model of the region
%				of uncertainty about the observation
%				due to various error sources such as noise.
%				Requires the presence of an ErrorModel
%				substructure with the following fields:
%				Statistic - 'varMA'|'var'
%					Moving average variance or
%					variance of the observations.
%				Window - Number of observations in a
%					moving average variance.
%				NormalCoverage - Set symmetric boundaries
%					about the observation such that
%					each direction covers .N of a
%					normal distribution.  (Note that
%					some Method.Type operations
%					may not need this culling parameter.
%			'asymptotic' - Use the asymptotic difference
%				of two cepstral signals (Merhav & Lee)
%				Expects an Asymptotic substructure with
%				the following fields.
%				Rho - Exponential decay rate (0,1]
%				C - scale factor (0,1]
%
% Cache.b(time, state) - Probability of observing Data(time,:) in
%	a given state.
% Cache.bmix(mixture, state, time) - Probability of observing
%	Data(time,:) in a given state and mixture.  (The bmix
%	field may be omitted, see optional arguments above.)
%
% This code is copyrighted 1997-2002 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 


error(nargchk(2,inf,nargin))

States = hmmModelStates(Model);
Mixtures = hmmModelMixtures(Model);

CacheMixtures = 1;		% defaults
Start=1;
[Time Components] = size(Data);
Method.Type = 'none';

m = 1;
while  m < length(varargin)	% process optional arguments
  switch varargin{m}
   case 'Mixtures'
    CacheMixtures=varargin{m+1}; m=m+2;
   case 'Observations'
    error('Observations argument no longer supported')
   case 'Method'
    Method = varargin{m+1}; m = m+2;
   otherwise, error(sprintf('Bad optional argument %s', varargin{m}));
  end
end

switch Method.Type
 case 'none'
  if CacheMixtures
    [Cache.b, Cache.bmix] = hmmObsProbDiag(Data', Model);
  else % no cache
    Cache.b = hmmObsProbDiag(Data', Model);
  end
  
 case 'closed_form'
  IntegralApprox = 2;	% pdf_ClosedForm from hmm/hmmlib.h
  
  switch Method.Region.Type
   case 'ErrorModel'
    BoundingVector = ...
	hmmErrorModelRegion(Method.Region.ErrorModel, Data);
   case 'asymptotic'
    BoundingVector = hmmAsymptotic(Method.Region.Asymptotic, Components);
  end
  
  if CacheMixtures
    [Cache.b, Cache.bmix] = ...
	hmmObsProbDiag(Data', Model, 0, IntegralApprox, BoundingVector);
  else
    Cache.b = hmmObsProbDiag(Data', Model, 0, IntegralApprox, BoundingVector);
  end
  
 case 'cubature'
  % Determine integration region

  IntegralApprox = 1;	% pdf_ObsCubature from hmm/hmmlib.h
  PCAComponents = Method.Cubature.PCA.Components;
  
  switch Method.Region.Type
   case 'ErrorModel'
    [BoundingVector, VarCov] = ...
	hmmErrorModelRegion(Method.Region.ErrorModel, Data);

    % Determine principal components.
    if PCAComponents > 0
      % Determine principal components based upon the estimated
      % variance-covariance matrix.
      [PCABasis EigVal] = ...
	  stPrincipalComponents('VarCovar', VarCov, ...
				'Method', Method.Cubature.PCA.Method, ...
				PCAComponents);
	    
      if PCAComponents < Components	% Remove unneeded components
	Discard = PCAComponents+1:Components;
	PCABasis(:,Discard) = [];
	EigVal(Discard) = [];
      end
	
      BasisVectors = PCABasis;
      
      % Establish the extent of the bounding cube in PCA space
      BoundingVector = BoundingVector * BasisVectors;
	
    else
      % No PCA - good luck!
      BasisVectors = eye(Components);
    end
    
   case 'asymptotic'
    
    % Size and shape of integration region
    C = Method.Region.Asymptotic.C;
    Rho = Method.Region.Asymptotic.Rho;
    BoundingVector = C * (Rho .^(1:PCAComponents) ./ (1:PCAComponents))';
    
    BasisVectors = eye(Components);

    if PCAComponents < Components	% Remove unneeded components
      % Zero covariance by assumption.  Variances in cepstrum
      % descend, so simply take the first Components Bases.
      Discard = PCAComponents+1:Components;
      BasisVectors(:, Discard) = [];
    end

  end % switch on region type

  % Regions decided, integrate.
  if CacheMixtures
    [Cache.b, Cache.bmix] = ...
	hmmObsProbDiag(Data', Model, 0, IntegralApprox, ...
		       BasisVectors, BoundingVector, Method.Cubature.Panels+1);
  else % no cache
    Cache.b = hmmObsProbDiag(Data', Model, 0, IntegralApprox, ...
			     BasisVectors, BoundingVector, Method.Cubature.Panels+1);
  end

end 



% ------------------------------------------------------------
function [BoundingVector, VarCov] = hmmErrorModelRegion(ErrorModel, Data)

switch ErrorModel.Statistic
 case 'var'
  VarCov = stVar(Data);
 case 'varMA'
  VarCov = stMAVarCov(Data, ErrorModel.Window);
 otherwise
  error('Bad statistic specified for ErrorModel region');
end

% Determine offset vector Eps for bounding cube.  As a simplification,
% assume covariances 0 in computation even if not quite true
%	(they are asymptotically).  

% Assume z ~ n(0,1).  Then Eps can be represented as a z-score by
%			    z = (Eps - mu) / sigma  (mu = mean, sigma = stddev)
% If we can solve for z, then Eps = z*sigma + mu. 

Std = sqrt(diag(VarCov));	% sample standard deviation 

% Determine z such that the corresponding offset k covers
% NormalCoverage percent of the distribution along each axis.
z = stNormCDFInv(.5 + ErrorModel.NormalCoverage / 2);

% Establish the extent of the bounding cube by converting
% the n(0,1) coverage to n(0, Std^2)
BoundingVector = z * Std';	% ' -> row vector

% ------------------------------------------------------------
function BoundingVector = hmmAsymptotic(Asymptotic, NumberComponents)

% Size and shape of integration region
Components = 1:NumberComponents;
BoundingVector = Asymptotic.C * (Asymptotic.Rho .^ Components ./ Components)';


