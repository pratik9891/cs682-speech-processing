function Beta = hmmBeta(Model, AlphaScaling, Data, Cache)
% Beta = hmmBeta(Model, AlphaScaling, Data, Cache)
% Computes a scaled beta matrix for observation data.
% See Rabiner & Huang _Fundamental of Speech Recognition_ 1993
% pp 365-7 for information on scaled beta values.
%
% Data should be a set of observerations where each column
% is a different component and the rows range from time 1 to T.
%
% Cache is an optional argument containing a structure of precomputed
% probabilites.  
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(3,4,nargin));

if nargin < 4
  Cache = hmmProbCache(Model, Data);
end


States = hmmModelStates(Model);
Time = size(Data, 1);	% Observations
Beta = zeros(Time, States);
BetaTrans = zeros(States, States);
Path = zeros(States);
StateInd = ones(States,1);	% for replicating matrices.

t = Time;

% Beta_{t,j} = is the backwards probability of observing Data(t) 
% in state j at time t.  For the last time unit, this is simply 1.
Beta(t, :) = repmat(AlphaScaling(t), [1, States]);

lag1 = t;
t = t - 1;

while t > 0

  % Observation probabilities for states 1..N
  b = Cache.b(:,lag1)';

  % BetaTrans_{i,j}:
  %
  %	Probability of transitioning backwards from state j
  %	to state i through all legal backward paths.
  %
  % i.e. for a 3 state HMM where B(N) = Beta(t+1,N):
  %		1			2			3
  %	1	a(1,1)B(1)Data(t+1,1)	a(1,2)B(2)Data(t+1,2)	a(1,3)B(3)Data(t+1,3)
  %	2	a(2,1)B(1)Data(t+1,1)	a(2,2)B(2)Data(t+1,2)	a(2,3)B(3)Data(t+1,3)
  %	3	a(3,1)B(1)Data(t+1,1)	a(3,2)B(2)Data(t+1,2)	a(3,3)B(3)Data(t+1,3)
  %
  Betalag1 = Beta(lag1, :);
  BetaTrans = Betalag1(StateInd,:) .* Model.a .* b(StateInd, :);
  
  % Beta(t,k) = sum over j Betatrans(k,j)
  Beta(t,:) = sum(BetaTrans, 2)' * AlphaScaling(t);

  lag1 = t;
  t = t-1;
end
