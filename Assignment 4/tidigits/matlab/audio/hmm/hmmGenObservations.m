function Observations = hmmGenObservations(Model, Length)
% Observations = hmmGenObservations(Model, Length)
% Generates an observation sequence from a model.
%
% Limitations:  When generating an observation for a state, we assume it
% is from a specific mixture.  Furthermore, the distribution from which
% we are choosing will be assumed to have indpendent components, even if
% dependencies exist in the variance/covariance matrix for that mixture.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 


error(nargchk(2,2,nargin));

States = hmmModelStates(Model);
Mixtures = hmmModelMixtures(Model);
Components = hmmModelObservationSpace(Model);

% Preallocate observations for speed
Observations = zeros(Length, Components);

% Compute cumulative distribution functions
picdf = Model.pi;
acdf = Model.a;
for i=2:States
  picdf(i) = picdf(i) + picdf(i-1);
  acdf(:,i) = acdf(:,i) + acdf(:,i-1);
end

State = hmmGenObsNextState(picdf);
t = 1;

while (t <= Length)
  Mix = floor(rand * Mixtures)+1;	% Pick mixture
  
  % Generate the observation
  Observations(t,:) = Model.Mix.mu(Mix,:,State) + ...
      sqrt(diag(Model.Mix.cov.Sigma{Mix,State}))' .* randn(1, Components);
  
  % Transition to next state
  State = hmmGenObsNextState(acdf(State,:));
  t=t+1;
end

% Auxilary functions ----------------------------------------

function State = hmmGenObsNextState(cdfVector)
% Given the cumulative density function for a state, pick the next state.
x = rand;
Indices = find(x < cdfVector);
State = Indices(1);





