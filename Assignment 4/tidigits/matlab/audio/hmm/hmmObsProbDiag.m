% [Probability, MixProbs] = ...
% hmmObsProbDiag(Observations, Model, State, IntegrationMethod, IntegrationsArgs)
%
% Computes probabilities of observing multidimensional
% observations in a either a specific state or for all states (if the
% optional state argument is not given or 0).  It is assumed that the
% covariance matrix for each mixture is diagonal.  
%
% Observations must be a Components X NumberObservations matrix. 
%
% Probability - States X NumberObservations
%		Will be a row vector if a single state is requested.
% MixProbs - Mixtures X States X Time
%
% Note:  We use a mixture of multivariate normal distributions.
%
%	f(x) = 2pi^{-n/2} det(Gamma)^{-1/2} * 
%		exp(-.5 (x-mu)' Gamma^{-1} (x-mu))
%
% The optional integration method specifies what type of integration should
% be used:
%	0, [MixturesToEvaluate]
%	    - No integration.  If the list MixturesToEvaluate is present,
%	      only the specificed mixtures will be evaluated.
%	1, [StandardDeviationVector]
%	    - Integration approximated by subdividing a local region into 
%	    2^components partitions centered at the current test observation.  
%	    Each partition is scaled by the value of the the model pdf in 
%	    that partition.  The size of the region is chosen such that
%	    .05 of the probability of a multidimensional Gaussian centered
%	    at the observation with a standard deviation as specified
%	    in a separate vector.
%	2, [EpsVector]
%	   - Integration approximated by integrating over a local region
%	     about each observation within +/- Eps.
%	
% For performance tracking purposes, one may call hmmObsProbDiag without any
% arguments and the number of pdf evaluations since the first time 
% hmmObsProbDiag was invoked will be returned.  The counter can be reset
% by typing clear hmmObsProbDiag.  Note that metrics are only kept 
% when integration is not performed.
%
% This code is copyrighted 1999-2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 
