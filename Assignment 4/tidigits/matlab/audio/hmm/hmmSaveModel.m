function hmmSaveModel(FileName, Model)
% Model = hmmSaveModel(FileName, Model)
% Writes a Hidden Markov Model to disk
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,2,nargin))

save(FileName, 'Model');
