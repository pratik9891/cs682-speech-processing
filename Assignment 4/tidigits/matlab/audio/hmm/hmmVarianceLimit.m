function Sigma = hmmVarianceLimit(Model)
% Sigma = hmmVarianceLimit(Model)
% Based upon the variance limit stored in the model, examines the covariance
% matrices of the model and generates a new covariance matrix in which
% no non-zero variance or covariance is beneath the limiting threshold.
%
% Note:  If the returned value is used to update the model, be sure to
% recompute constants with hmmPrecompute().
%
% This code is copyrighted 1997-2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

States=hmmModelStates(Model);
Mixtures=hmmModelMixtures(Model);

Sigma = Model.Mix.cov.Sigma;
Modifications=0;

for StIdx=1:States
  for MixIdx=1:Mixtures

    % Find variances to floor
    FloorIndices = find(diag(Sigma{MixIdx, StIdx}) < Model.Mix.LimitFloors);
    if ~ isempty(FloorIndices)
      DiagIndices = (FloorIndices - 1) * Model.Components + FloorIndices;
      % Floor variances
      Sigma{MixIdx,StIdx}(DiagIndices) = Model.Mix.LimitFloors(FloorIndices);
      Modifications=1;
    end
  end
end

if Modifications	% temporary just so we can see how often this is done
  fprintf('+')
end


