/*
 * hmmPersistentInit.c
 *
 * Set up an array for holding C representation of HMM models
 * across Mex invocations.
 *
 * This code is copyrighted 2003 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */


#include <mex.h>
#include <matrix.h>
#include <math.h>

#include <dlfcn.h>

#include <sys/types.h>
#include <unistd.h>

#include "lib/hmmlib.h"
#include "lib/soException.h"

#include <utSharedLib.h>


/*
 * Matlab gateway function hmmObsProbDiag()
 * See hmmPersistentInit.m for documentation
 */

void
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  /* parameter positions  ----------------------------------------*/
  
  /* in */
  const int	InModelCount = 0;
  const int	InCount = 1;
    
  int		ModelCount;
  void		*ModelArray;
  
  void		*soLib = NULL;
  HMMCache	*ModelCache;
  char		*error;

  SET_FN	set;

  if (nrhs < InCount || nrhs > InCount)
    mexErrMsgTxt("Invalid arguments");

  if (mxIsDouble(prhs[InModelCount]) &&
      mxGetM(prhs[InModelCount]) * mxGetN(prhs[InModelCount]) == 1) {

    ModelCount = mxGetScalar(prhs[InModelCount]);

    /* Open library */
    soLib = dlopen(UTSHAREDLIB, RTLD_LAZY);
    soThrowIfException(soLib);

    set = dlsym(soLib, "set");	/* get function handle */
    soThrowIfException(soLib);

    /* Allocate array
     * Note that we do not use the Matlab mx functions to allocate,
     * they would be deallocated as soon as we left the Mex file
     */
    if (! (ModelCache = calloc(1, sizeof(HMMCache))))
      soThrowException(soLib, "Unable to allocate HMM cache");
    if (! (ModelCache->HMM = calloc(ModelCount, sizeof(HMM))))
      soThrowException(soLib, "Unable to allocate array for HMM cache");
    ModelCache->N = ModelCount;
    
    set(HMMSEGMENT, ModelCache);	/* store memory block */
    
    /* Close library */
    dlclose(soLib);
    
  } else {
    mexErrMsgTxt("Argument must be a scalar");
  }
}
    

    
