function Covariance = hmmEstimateCov(Data, Independent, Clamp)
% Covariance = hmmEstimateCov(Data, Independent, Clamp)
% Given a set of row vector data, estimate the sample covariance.
% If Independent is non-zero, only the components are assumed to be
% independent and only the diagonals are computed.  If Clamp is
% non-zero, variance clamping is performed.  No value on the
% diagonal is permitted to fall below the threshold specified
% by Clamp.  This can be used to prevent limited sample data from
% having too small a variance.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

if nargin < 3
  Clamp = 0;
  if nargin < 2
    Independent = 0;
  end
end

if Independent
  % Even though we can represent this as a vector, we'll use
  % a full matrix implementation so that clamping does not become
  % a special case.  Later, we'll convert to a sparse matrix form.
  Covariance = diag(var(Data));
else
  Covariance = cov(Data);
end
  
if Clamp
  if ~ Independent
    Variance = diag(Data);
  end
  
  % We find which diagonal elements below the clamping threshold.
  Indices = find(Variance < Clamp);
  % Convert the indices of these diagonals into offsets into a
  % one dimensional array representation of the covariance matrix
  % and then set those elements to the clamping threshold.
  Columns = size(Covariance,2);
  Elements = (Indices - 1) * Columns + Indices;
  Covariance(Elements) = Clamp;
end

if Independent
  % Convert to a sparse matrix for efficiency
  Covariance = spdiags(diag(Covariance), 0, size(Covariance, 1), ...
      size(Covariance, 2));
end


