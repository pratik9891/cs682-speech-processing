function Distance = hmmIntegrityCheck(Model)
% hmmIntegrityCheck(Model)
% Evaluate how well model training works by taking a model and generating an
% observation sequence from it.  Use this sequence to train a new model with
% the same parameters as the original and compute the distance between the
% two models.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

% 30 seconds of data assuming a window shift size of 16 ms.
Length=1875;	
Obs = hmmGenObservations(Model, Length);

% Create a new model
NInfo = Model.Info;
NInfo.Utterances = 'synth';	% mark as synthetic data
NModel = hmmCreateModel(Model.NumberStates, Model.NumberMixtures, Obs, ...
    NInfo);

% Check similarity between models.
Distance = hmmModelDistance(Model, NModel);
