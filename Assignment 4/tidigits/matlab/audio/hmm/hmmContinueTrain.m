function Model = hmmContinueTrain(Model, TrainingData)
% Model = hmmContinueTrain(Model, TrainingData)
% Given a model and a set of training data, continue trianing.

Model = hmmBaumWelchIteration(Model, TrainingData);
