/*
 * hmmViterbiAux.c
 *
 * Given a model, and a set of cached probabilities corresponding to
 * the observations with respect to the states of the model, determine
 * the best possible path through the model and the log likelihood of
 * that path occuring.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited.
 */

#include <mex.h>
#include <matrix.h>
#include <math.h>
#include "lib/hmmlib.h"
#include "lib/hmmModelAccessor.h"

/*
 * Matlab gateway function hmmViterbiCachedLikelihoods()
 * See hmmViterbiCachedLikelihoods.m for documentation
 */

void
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  /* parameter positions  ----------------------------------------*/

  /* in */
  const int	ModelPos = 0;
  const int	StateLikelihoodsPos = 1;
  const int	InputArgCount = 2;

  /* out */
  const int	OutLikelihoodPos = 0;
  const int	OptPathPos = 1;
  const int	OutputArgCount = 2;

  HMM		Model;
  double	*StateLikelihoodPtr, *OptimalStateSeqPtr;
  double	LogLikelihood;
  int		i, T;

  
  
  if (nrhs < InputArgCount)
    mexErrMsgTxt("Bad number of arguments.");

  hmmModelAccessor(prhs[ModelPos], &Model, ALLOC_RETRIEVE);

  if (mxGetM(prhs[StateLikelihoodsPos]) != Model.States)
    mexErrMsgTxt("Cached likelihoods inappropriate for number "
		 "of states in model.");
  
  StateLikelihoodPtr = mxGetPr(prhs[StateLikelihoodsPos]);

  T = (int) mxGetN(prhs[StateLikelihoodsPos]);

  plhs[OutLikelihoodPos] = mxCreateDoubleMatrix(1, 1, mxREAL);

  if (nlhs > OptPathPos) {
    /* user wants optimal state path */
    plhs[OptPathPos] = mxCreateDoubleMatrix(T, 1, mxREAL);
    OptimalStateSeqPtr = mxGetPr(plhs[OptPathPos]);
  } else {
    OptimalStateSeqPtr = NULL;
  }
    
  LogLikelihood = hmmViterbi(&Model, StateLikelihoodPtr,
			     T, OptimalStateSeqPtr);

  /* hmmViterbi uses a return value of infinity to signal error */
  if (mxIsInf(LogLikelihood)) {
    mexErrMsgTxt("libhmmmex:hmmViterbi - Either bad model structure, "
		 "bad data, or memory allocation failure.");
  }
  
  *(mxGetPr(plhs[OutLikelihoodPos])) = LogLikelihood;

  if (OptimalStateSeqPtr) {
    /* convert 0:N-1 indexing to 1:N */
    for (i=0; i < T; i++)
      OptimalStateSeqPtr[i]++;
  }

  /* free resources */
  hmmFreeModelAccessor(&Model, ALLOC_MATLAB);
}

  

