function [Likelihood, LikelihoodGeomMean]= hmmAlphaProb(AlphaScale)
% [Likelihood LikelihoodGeomMean] = hmmAlphaProb(AlphaScaling)
% Computes the log likelihood score and its geometric mean (also on a log
% scale) based upon the alpha scalings generated by a specific model and
% observation.
%
% See also:  hmmAlpha
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

Likelihood = - sum(log(AlphaScale));
LikelihoodGeomMean = Likelihood / length(AlphaScale);

