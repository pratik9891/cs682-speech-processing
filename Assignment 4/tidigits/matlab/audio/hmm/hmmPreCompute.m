function Model = hmmPreCompute(Model)
% Model = hmmPreCompute(Model)
% There are numerous functions in an HMM which can be precomputed
% to speed calculations, such as taking determinants and computing
% inverses.  This function perfroms such calculations and stores
% them in the model.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

IntegrityChecks = 0;
if IntegrityChecks
  NonUnityRows = find(sum(Model.a, 2) ~= 1);
  if ~ isempty(NonUnityRows)
    fprintf('Rows:  ');
    fprintf('%d ', NonUnityRows);
    fprintf('of transition matrix do not sum to 1:\n');
    Model.a
    sum(Model.a,2)
  end
  
  if (sum(Model.pi) ~= 1)
    fprintf('Pi does not sum to 1\n');
    Model.pi
    sum(Model.pi)
  end
  
  NonUnityRows = find(sum(Model.Mix.c, 1) ~= 1);
  if ~ isempty(NonUnityRows)
    fprintf('Rows:  ');
    fprintf('%d ', NonUnityRows);
    fprintf('of mixture scale matrix do not sum to 1:\n');
    Model.Mix.c
    sum(Model.Mix.c,2)
  end
  
end


% Preallocate space
Model.Mix.cov.SigmaInv = cell(size(Model.Mix.cov.Sigma));	
Model.Mix.cov.DetSigma = zeros(Model.NumberMixtures, Model.NumberStates);

% Compute inverses and determinants
for m=1:Model.NumberMixtures
  for s=1:Model.NumberStates
    if Model.Mix.cov.Diagonal
      Model.Mix.cov.SigmaInv{m,s} = ...
          spdiags(1 ./ spdiags(Model.Mix.cov.Sigma{m,s}, 0), ...
                  0, Model.Components, Model.Components);
      Model.Mix.cov.DetSigma(m,s) = ...
          prod(spdiags(Model.Mix.cov.Sigma{m,s}, 0));
    else
      Model.Mix.cov.SigmaInv{m,s} = inv(Model.Mix.cov.Sigma{m,s});
      Model.Mix.cov.DetSigma(m,s) = det(Model.Mix.cov.Sigma{m,s});
    end
  end
end


% Compute pdf normalization constant
%
% NumDim = hmmModelObservationSpace(Model);
% Model.Mix.cov.k = (2*pi)^(-NumDim/2) * (Model.Mix.cov.DetSigma .^ -.5);
%
% No need to include non model specific term (2pi)^(-NumDim/2)
% as all models have same value and won't affect maximum.
Model.Mix.cov.k = (Model.Mix.cov.DetSigma .^ -.5);
Model.Mix.cov.logk = log(Model.Mix.cov.k);

