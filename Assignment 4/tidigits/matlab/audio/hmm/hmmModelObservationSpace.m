function Components = hmmmodelobservationspace(Model)
% Components = hmmModelObservationSpace(Model)
% Reports the number of dimensions represented by a Hidden Markov Model.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

Components = size(Model.Mix.cov.Sigma{1,1}, 1);
