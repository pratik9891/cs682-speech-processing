function Model = hmmSegmentalKMeans(Model, TrainingData)
% Model = hmmSegmentalKMeans(Model, TrainingData)
% Performs a Segmental K Means HMM model estimate for the given
% training data.
%
% This is useful as a preliminary estimation of a continuous HMM model
% with multiple mixtures.  Once the preliminary "good" estimate is
% established, it can be refined using other means such as the
% Expection/Maximization algorithm (i.e. Baum-Welch reestimation).
%
% The algorithm basically performs the following:
%	
% Given an initial estimation (possibly random), the observation
% (training) sequence is partitioned into states based upon the best
% state path through the model as determined by the Viterbi algorithm.
%
% The observation vectors associated with each state are then
% clustered using vector-quantization into k codewords where k is the
% number of mixtures associated with each state.
%
% The model is updated based upon the clustering and the process is
% repeated until the termination condition is reached.
%
% Note that in the degenerate case of a single state model, only one
% iteration will occur as the state partitioning will never change.
%
% This code is copyrighted 1997-2002 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,2, nargin));

Verbosity = 0;

MixtureOutlierSize = 10;	% Clusters size <= N are considered outliers.

[observations, components] = size(TrainingData);
states = Model.NumberStates;
mixtures = Model.NumberMixtures;

% Find the optimum path through the current model.
[Prob, Path] = hmmViterbi(Model, TrainingData);
if Verbosity
  fprintf('hmmSegmentalKMeans - - Initial p(TrainData)=%f\n', ...
	  exp(Prob/observations));
end

MaxCount = 4;			% Don't loop more than N times.
DistanceThreshold = -.1;	% symmetric distance measure threshold

Distance = -Inf;
Count = 0;
while Distance < DistanceThreshold & Count < MaxCount
  NewModel = Model;
  
  % Recompute transition matrix
  A = zeros(states, states);
  tprev = 1;
  for t=2:observations
    A(Path(tprev),Path(t)) = A(Path(tprev),Path(t)) + 1;
    tprev = t;
  end
  RowSums = sum(A,2);
  Indices = find(RowSums == 0);
  if (Indices)
    warning(['hmmSegmentalKMeans - State(s) ', sprintf('%d ', Indices), ...
	  'can no longer be reached.\n']);
    RowSums(Indices) = 1;	% Avoid /0
  end
  NewModel.a = A ./ RowSums(:, ones(1,states));
  
  % Create a k-means codebook for each state
  if Verbosity; fprintf('Generating state specific codebooks\n'); end
  for s=1:states

    % Find the training vectors which passed through state s and construct a
    % mixture word code book for all of those states.
    StateTrainingIndices = find(Path == s);
    if ~ isempty(StateTrainingIndices)
      StateTrainingData = TrainingData(StateTrainingIndices, :);

      % Build codebook for this mixture, eliminating any outliers
      % Elimination of outliers is an ad-hoc heuristic.
      Done = 0;
      Iteration = 0;
      MaxOutlierPasses = 3;
      while ~ Done & Iteration < MaxOutlierPasses;
	
	% Build Mixture word codebook 
	[Codebook, Indices] = ...
	    vqCreateModel(mixtures, StateTrainingData, ...
			  'K-means', 'lbg', ...
			  'Distortion', Model.Info.DistortionMetric, ...
			  'Verbosity', Verbosity);

        % Ad hoc outlier removal, useful for preventing stability
        % problems which can occur when a mixture is only assigned
        % a couple of samples
	ObservationsPerMixture = histc(Indices, [1:mixtures]);
	MixturesWithOutliers = find(ObservationsPerMixture <= ...
				    MixtureOutlierSize)';
	if isempty(MixturesWithOutliers)
	  Done = 1;
	elseif Iteration < MaxOutlierPasses - 1
	  OldTrainingCount = size(StateTrainingData, 1);
	  Remove = [];
	  for m = MixturesWithOutliers
	    Remove = union(Remove, find(Indices == m));
	  end
	  StateTrainingData(Remove,:) = [];

	  fprintf('State %d mixture(s) ', s);
	  fprintf('%d ', MixturesWithOutliers);
	  fprintf(': <= %d vectors associated with these mixtures.\n\tCounts ', ...
		  MixtureOutlierSize);
	  for m=MixturesWithOutliers
	    fprintf('mix(%d)=%d, ', m, ObservationsPerMixture(m))
	  end
	  fprintf(['\nTreating %d observation(s) as outliers and ', ...
		   'regenerating mixture codebooks\n'], ...
		  OldTrainingCount - size(StateTrainingData, 1));
	  
	end
	Iteration = Iteration + 1;
      end

      if ~ Done
	warning([sprintf('State %d mixture(s) ', s), ...
	      sprintf('%d ', MixturesWithOutliers), ...
	      sprintf([' still have <= %d vectors associated with these ', ...
		      'mixtures.  Proceeding anyway.'], MixtureOutlierSize)]);
      end
      
      % Weight the mixtures by relative frequency.
      NewModel.Mix.c(:,s) = ...
	  ObservationsPerMixture' / size(StateTrainingData,1);

      % Set mixture means to the Mixture word code book
      %NewModel.Mix.mu(:,:,s) = Codebook.CodeWords;

      for m=1:mixtures
        % Estimate the mean for this mixture
        NewModel.Mix.mu(m,:,s) = ...
            mean(StateTrainingData(find(Indices == m),:));
	
	% Estimate the variance for the codewords associated with a mixture.
	% If there are too few observations associated with the codeword,
	% our variance estimate will be bad.  
	%
	% If 0 or 1, covariance estimate will be bad, generate an error.
	% If < heuristic value, warn the user know about it and continue.
	if ObservationsPerMixture(m) > 1
	  
	  % One or more observations quantized to this mixture
	  NewModel.Mix.cov.Sigma{m,s} = ...
	      hmmEstimateCov(StateTrainingData(find(Indices == m)',:), ...
			     NewModel.Mix.cov.Diagonal);
	else
	  error(sprintf(['Unable to estimate variance as only one ' ...
			 'training sample assigned to mixture %d\n', ...
			 'There may be too many mixtures too reliably ', ...
			 'estimate the %d training vectors\n', ...
			 'associated with this state.\n'], ...
			m, size(StateTrainingData, 1)));
	end
      end
    end
  end
  
  if (NewModel.Mix.limit > 0)
    % Make sure that variances do not exceed limits
    NewModel.Mix.cov.Sigma = hmmVarianceLimit(NewModel);
  end

  NewModel = hmmPreCompute(NewModel);
  
  % Find the optimum path through the current model.
  [Prob, Path] = hmmViterbi(NewModel, TrainingData);
  
  if (states == 1)
    % When there is only one state, segmentation of the training data by
    % state will not change.  Set Distance such that the loop invariant no
    % longer holds and we exit.
    Distance = 0;
  else
    Distance = hmmModelDistance(NewModel, Model);
  end
  
  Model = NewModel;
  Count = Count + 1;
  
  if Verbosity
    fprintf('hmmSegmentalKMeans - Iteration %d, p(TrainData)=%f, Dist %f\n', ...
	    Count, exp(Prob/observations), Distance);
  end

end

