/*
 * hmmObsProbDiag.c
 *
 * Compute the probability of an observation given a model
 * with diagonal variance/covariance matrices.
 *
 * This code is copyrighted 1997-2002 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#include <mex.h>
#include <matrix.h>
#include <math.h>
#include "lib/hmmlib.h"
#include "lib/hmmModelAccessor.h"

/* total number of pdf evaluations since library loaded (for statistics) */
static double pdfCount = 0;

/*
 * Matlab gateway function hmmObsProbDiag()
 * See hmmObsProbDiag.m for documentation
 */

void
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  /* parameter positions  ----------------------------------------*/

  /* in */
  const int	ObsPos = 0;
  const int	ModelPos = 1;
  const int	AllStatesPos = 2;
  const int	MethodPos = 3;

  /* method dependent arguments: */

  /* pdf_Point */
  const int	EvaluateSpecificMixturesPos = 4;
  
  /* ClosedForm */
  const int	ClosedBoundingVectorPos = 4;
  
  /* ObsCubature */
  const int	CubatureBasisPos = 4;
  const int	CubatureBoundingVectorPos = 5;
  const int	CubatureEvaluationsPos = 6;
  
  /* EpsVector */
  const int	EpsVectorPos = 4;
  const int	ExtentVectorPos = 5;
  /* EpsMatrixQuad */
  const int	EpsMatrixPos = 4;
  const int	ExtentMatrixPos = 5;
  const int	IntegrationPanelsPos = 6;

  /* out */
  const int	f_StatePos = 0;
  const int	f_MixPos = 1;

  /* locals ---------------------------------------- */
  double const	*Obs, *Obs_t;

  int		s, StateStart, StateStop, StatesToEvaluate;
  int		t, T;
  int		MixDims[3];

  HMM		Model;
  mxArray	*Mx_f_Mixtures, *Mx_f_States;
  double	*f_MixPtr, *f_StatePtr;
  pdfMethod	Method;
  int		Error = 0, Allocated = 0;
  char		*Msg;

  int		ReturnMixtures;		/* Caller wants mixture probs? */
  
  /* Minimal error handling - should not be invoked directly by the user */

  /* return number of pdfs evaluated if invoked with no arguments */
  if (nrhs == 0) {
    if (nlhs == 1) {
      plhs[0] = mxCreateDoubleScalar(pdfCount);
      return;
    }
  }

  /* Find out if the user wants the mixture probabilities returned */
  ReturnMixtures = (nlhs > f_MixPos);
  
  /* Retrieve data */

  Obs = mxGetPr(prhs[ObsPos]);
  hmmModelAccessor(prhs[ModelPos], &Model, ALLOC_RETRIEVE);

  /*
   * Check if model and observation have same number of components.
   * model is valid, this is the only check we should require.
   */
  if (mxGetM(prhs[ObsPos]) != Model.Components) {
    hmmFreeModelAccessor(&Model, ALLOC_MATLAB);
    mexErrMsgTxt("Size mismatch between observation and Model dimensions");
  }
  T = mxGetN(prhs[ObsPos]);	/* Number of observations */

  /* Determine whether the user requested all states by omitting
   * the argument or providing 0, or if she requested evaluation
   * of a specific state.
   */
  StateStart = (nrhs > AllStatesPos)?
    (int) mxGetScalar(prhs[AllStatesPos]) : 0;
  if (StateStart) {
    /* Evaluate a single state */
    StateStart--;	/* Matlab start 1, C/C++ start 0 */
    StateStop = StateStart;
  } else 
    StateStop = Model.States - 1;	/* evaluate all states */
  StatesToEvaluate = StateStop - StateStart + 1;
  
  /* Determine what evaluation method to use or default to point evaluation */
  Method = (nrhs > MethodPos) ?
    (int) mxGetScalar(prhs[MethodPos]) : pdf_Point;

  /* Allocate space for state probabilities */
  Mx_f_States = mxCreateDoubleMatrix(StatesToEvaluate, T, mxREAL);
  f_StatePtr = mxGetPr(Mx_f_States);
  
  /* Allocate space for individual mixture results.
   * If the user doesn't want this returned, only allocate enough
   * space for one pdf evaluation at a time and free the memory
   * before returning.
   */
  if (ReturnMixtures) {
    MixDims[0] = Model.Mixtures;
    MixDims[1] = StatesToEvaluate;
    MixDims[2] = T;
    Mx_f_Mixtures = mxCreateNumericArray(3, MixDims, mxDOUBLE_CLASS, mxREAL);
  } else {
    Mx_f_Mixtures = mxCreateDoubleMatrix(1, Model.Mixtures, mxREAL);
  }

  if (Mx_f_Mixtures)
    Allocated = 1;
  else
    mexErrMsgTxt("Unable to allocate space for mixture results.");
  f_MixPtr = mxGetPr(Mx_f_Mixtures);

  Obs_t = Obs;
  
  switch (Method) {
  case pdf_Point:
    {
      int	SpecificMixtureCount;
      double	*MixtureListPtr;

      if (nrhs > EvaluateSpecificMixturesPos) {
	/* User only wants some of the mixtures evaluated.  Pull in
	 * the list of mixtures.  ASSUME THAT THEY ARE SORTED.  If
	 * not, it won't crash, but the results won't be right.
	 */
	SpecificMixtureCount = mxGetM(prhs[EvaluateSpecificMixturesPos]);
	if (mxGetN(prhs[EvaluateSpecificMixturesPos]) != T)
	  mexErrMsgTxt("There must be a mixture list for each observation"); 
	MixtureListPtr = mxGetPr(prhs[EvaluateSpecificMixturesPos]);
	/* performance metric */
	pdfCount += T * SpecificMixtureCount * (StateStop - StateStart + 1);
      } else {
	SpecificMixtureCount = 0;
	/* performance metric */
	pdfCount += T * Model.Mixtures * (StateStop - StateStart + 1);
      }
      
      for (t = 0; t < T; t++) {
	for (s = StateStart; s <= StateStop; s++) {
	  if (SpecificMixtureCount) {
	    *f_StatePtr =
	      hmm_f_state_mixture_list(s, &Model, f_MixPtr, Obs_t,
				       SpecificMixtureCount,
				       MixtureListPtr);
	    /* Move to next list of mixtures to evaluate */
	    MixtureListPtr += SpecificMixtureCount;
	  } else
	    *f_StatePtr = hmm_f_state(s, &Model, f_MixPtr, Obs_t);
	  
	  /* Set pointers for next state */
	  if (ReturnMixtures) {
	    /* Only move to the next set of mixture probabilities
	     * if caller requested to have them returned.  Otherwise
	     * just use the same one over and over.
	     */
	    COL_INCCOL(f_MixPtr, Model.Mixtures, Model.States);
	  }
	  COL_INCROW(f_StatePtr, StatesToEvaluate, 1);
	}
	/* Set pointers for next observation */
      COL_INCCOL(Obs_t, Model.Components, T);
      }
    }
    break;

  case pdf_ObsCubature:
    {
      BasisSet		Basis;
      CubatureInfo	Info;
      const double *	BoundingVector;
      int		Evaluations, BoundingM, BoundingN;

      Basis.Components = mxGetM(prhs[CubatureBasisPos]);
      Basis.BasisCount = mxGetN(prhs[CubatureBasisPos]);
      Basis.Basis = mxGetPr(prhs[CubatureBasisPos]);
      BoundingVector = mxGetPr(prhs[CubatureBoundingVectorPos]);
      BoundingM = mxGetM(prhs[CubatureBoundingVectorPos]);
      BoundingN = mxGetN(prhs[CubatureBoundingVectorPos]);
      Evaluations = (int) mxGetScalar(prhs[CubatureEvaluationsPos]);

      if (Basis.Components != Model.Components) {
	Error = 1;
	Msg = "Basis has incompatible number of components with model.";
      } else if (! (BoundingM * BoundingN == Basis.BasisCount &&
		    (BoundingM == 1 || BoundingN == 1))) {
	Error = 1;
	Msg = "Cubature - Bounding vector has incompatible number of "
	  "components with model.";
      }
      if (! Error) {
	hmmCubatureInit(&Info, &Basis, BoundingVector, Evaluations);

	for (t = 0; t < T; t++) {
	  for (s = StateStart; s <= StateStop; s++) {
	    *f_StatePtr = hmmCubature(s, &Model, &Basis,
				      &Info, BoundingVector,
				      Obs_t, f_MixPtr);
	    
	    /* Set pointers for next state */
	    if (ReturnMixtures) {
	      /* Only move to the next set of mixture probabilities
	       * if caller requested to have them returned.  Otherwise
	       * just use the same one over and over.
	       */
	      COL_INCCOL(f_MixPtr, Model.Mixtures, Model.States);
	    }
	    COL_INCROW(f_StatePtr, StatesToEvaluate, 1);
	  }
	  /* Set pointers for next observation */
	  COL_INCCOL(Obs_t, Model.Components, T);
	}
      } /* end if (! Error) */
    }
    break;

  case pdf_ClosedForm:
    {
      const double *	BoundingVector;
      int		BoundingM, BoundingN;

      BoundingVector = mxGetPr(prhs[ClosedBoundingVectorPos]);
      BoundingM = mxGetM(prhs[ClosedBoundingVectorPos]);
      BoundingN = mxGetN(prhs[ClosedBoundingVectorPos]);
      
      if (Model.Components != BoundingM * BoundingN) {
	Error = 1;
	Msg = "Bounding vector of different size than model"
	  "observation space.";
      } else {
	for (t = 0; t < T; t++) {
	  for (s = StateStart; s <= StateStop; s++) {
	    *f_StatePtr = hmmFundThmCalc(s, &Model,
				      BoundingVector,
				      Obs_t, f_MixPtr);
	    
	    /* Set pointers for next state */
	    if (ReturnMixtures) {
	      /* Only move to the next set of mixture probabilities
	       * if caller requested to have them returned.  Otherwise
	       * just use the same one over and over.
	       */
	      COL_INCCOL(f_MixPtr, Model.Mixtures, Model.States);
	    }
	    COL_INCROW(f_StatePtr, StatesToEvaluate, 1);
	  }
	  /* Set pointers for next observation */
	  COL_INCCOL(Obs_t, Model.Components, T);
	}
      }
    }
    break;
    
  default:
    Error = 1;
    Msg = "Unrecognized integration method argument";
    break;
  } /* end switch */

  hmmFreeModelAccessor(&Model, ALLOC_MATLAB);

  if (Error) {
    if (Allocated)
      mxDestroyArray(Mx_f_Mixtures);
    mexErrMsgTxt(Msg);
  }

  /* Populate results structures */
  plhs[f_StatePos] = Mx_f_States;
  
  if (ReturnMixtures) {	/* Mixtures requested by caller? */
    plhs[f_MixPos] = Mx_f_Mixtures;	/* yes, return */
  } else {
    mxDestroyArray(Mx_f_Mixtures);	/* no, remove */
  }
}
  

