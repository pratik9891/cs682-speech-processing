function hmmPrintModel(Model)
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

fprintf('HMM Model:  %d states, %d mixtures, obs dim %d\n', ...
    Model.NumberStates, Model.NumberMixtures, size(Model.Mix.mu, 2));
fprintf('Pi - Initial probabilities\n');
Model.pi
fprintf('A - Transition matrix\n');
Model.a
fprintf('Mixture weights:  Mix X State\n');
Model.Mix.c
fprintf('Mixture means:  Mix X Component X State\n');
Model.Mix.mu
fprintf('Covariance information:  Comp X Comp X Mix X State\n');

for s=1:Model.NumberStates
  for m=1:Model.NumberMixtures
    fprintf('Cov(State=%d,Mix=%d)\n', s,m);
    Model.Mix.cov.Sigma{m,s}
  end
end
