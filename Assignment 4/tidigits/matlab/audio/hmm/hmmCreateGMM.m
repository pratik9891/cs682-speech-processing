function Model = hmmCreateGMM(N, Features, MaxIterations)
% Model = hmmCreateGMM(N, Features, [MaxIterations])
% Create an N mixture Gaussian mixture model.
% Features should be row oriented feature vectors.
% 
% Optional argument MaxIterations controls the maximum
% number of iterations of the expectation-maximization
% algorithm.  Defaults to 5.
%
% Returns a model that can be scored using 
% hmmViterbi(Model, TestFeatures)

narginchk(2,3);  % Verify # of inputs

if nargin == 2
    Info.MaximumIterations = 5;
else
    Info.MaximumIterations = MaxIterations;
end
% Create a degenerate hidden Markov model
Model = hmmCreateModel(1, N, Features, Info);
