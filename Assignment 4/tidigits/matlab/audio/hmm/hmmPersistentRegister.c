/*
 * hmmPersistentRegister.c
 *
 * Register an HMM model for access across Mex file invocations.
 *
 * This code is copyrighted 2003 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */


#include <mex.h>
#include <matrix.h>
#include <math.h>

#include <dlfcn.h>

#include <sys/types.h>
#include <unistd.h>

#include "lib/hmmlib.h"
#include "lib/soException.h"

#include <utSharedLib.h>
#include <MxUtil.h>


/* Name of field that will indicate model index */
#define PERSISTENTIDX "PersistentIdx"

/*
 * Matlab gateway function hmmPersistentRegister()
 * See hmmPersistentRegister.m for documentation
 */

void
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  /* parameter positions  ----------------------------------------*/
  
  /* in */
  const int	InModel = 0;
  const int	InCount = 1;
    
  int		ModelIndex;
  int		Status = 0;	/* default success */
  void		*ModelArray;
  
  void		*soLib = NULL;
  HMMCache	*ModelCache;
  HMM		*HMMEntry;
  char		*error;
  char		ErrorString[MAXSTRING];
  mxArray	*MxPersistentIndex;

  GET_FN	get;

  if (nrhs < InCount || nrhs > InCount)
    mexErrMsgTxt("Invalid arguments");

  /* Grab index */
  MxAccessField(prhs[InModel], MxPersistentIndex,
		PERSISTENTIDX, ErrorString);

  /* MxPersistentIndex contains index field,
   * make sure populated correctly
   */
  if (! mxIsDouble(MxPersistentIndex) ||
      mxGetM(MxPersistentIndex) * mxGetN(MxPersistentIndex) != 1) {
    sprintf(ErrorString, "Bad %s field in structure", PERSISTENTIDX);
    mexErrMsgTxt(ErrorString);
  }

  ModelIndex = mxGetScalar(MxPersistentIndex);

  /* Allocate space for model */
  if (! (HMMEntry = calloc(1, sizeof(HMM))))
    mexErrMsgTxt("Unable to allocate HMM model");

  /* Convert the model into something accessible from C
   * This is done before opening the library as the function
   * will call mexErrMsgTxt upon error, which would not
   * close the libraries
   */
  hmmModelAccessor(prhs[InModel], HMMEntry, ALLOC_CLIB);

  /* Open library */
  soLib = dlopen(UTSHAREDLIB, RTLD_LAZY);
  soThrowIfException(soLib);

  get = dlsym(soLib, "get");	/* get function handle */
  soThrowIfException(soLib);

  /* Retrieve pointer array */
  ModelCache = get(HMMSEGMENT);

  /* Close library */
  dlclose(soLib);

  /* Store the model */
  if (ModelCache) {
    if (ModelIndex > ModelCache->N || ModelIndex < 0) {
      hmmFreeModelAccessor(HMMEntry, ALLOC_CLIB);
      mexErrMsgTxt("Unable to register model, invalid index");
    } else {
      ModelCache->HMM[ModelIndex] = HMMEntry;
    }
  }
}
    

    
