#include <mex.h>
#include <matrix.h>
#include <stdio.h>
#include <math.h>

#include "lib/vqlib.h"

/* Optimization of variance reestimation for independent components */

#define MAXERRMSG	255
#define VEC_COMPONENT_INC	1	/* vectors - next component offset=1 */

/* gateway function
 *
 * LHS Arguments:
 *	Distortion
 *	CodewordIndex
 * RHS Arguments:
 *	Codebook
 *      Vector
 *
 * Given a vector quantizer CodeBook, and a single vector Vector,
 * find the minimum distortion encoding.  Distortion represents the
 * distance to the vector, and CodeWordIndex is the code word
 * which produced the minimum distortion.
 *
 * Distortion is a Euclidean distortion measure.  If a component
 * weighting is present in the CodeBook, it will be used in the
 * distortion computation.
 *
 * Note that in order to optimize this heavily used routine, no error
 * checking is performed.  Any error checking should be done at higher
 * levels, and this routine should not be invoked by the user without
 * caution.
 *
 * This code is copyrighted 1999-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */


void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray 
	    *prhs[])
{
  /* positional parameters */
  const int	CodeBookPos = 0;
  const int	VectorPos = 1;
  const int	InputArgCount = 2;
  
  const int	DistortionPos = 0;
  const int	CodewordIndexPos = 1;

  CodeBook	CB;
  double const	*Vector, *Weights;

  mxArray	*MxMinDistortion;

  double	Distortion, MinDistortion;
  int		MinCodeWord;
  int		Idx;
    
  /* check args */
  if (nrhs > InputArgCount)
    mexErrMsgTxt("Bad argument count");

  /* Retrieve CodeBook */
  vqCodeBookAccessor(prhs[CodeBookPos], &CB);

  /* Retrieve vector to encode */
  Vector = mxGetPr(prhs[VectorPos]);

    
  /* check vector is same size as codewords, we don't really
   * care if it is row or column vector as long as we have
   * the right number of elements.  For efficiency, we don't
   * verify that this is really a vector; a matrix with the right
   * number of elements will be accepted as well.
   */
  if (CB.VectorSize != (mxGetM(prhs[VectorPos]) * mxGetN(prhs[VectorPos]))) {
    char Message[MAXERRMSG];
    
    sprintf(Message, "Codebook/Vector size mismatch:  "
		      "Codebook: %d x %d, Vector %d x %d",
		      CB.CodeWordCount, CB.VectorSize,
		      mxGetM(prhs[VectorPos]),
		      mxGetN(prhs[VectorPos]));
    mexErrMsgTxt(Message);
  }
  
  /* compute distortion */
  MinDistortion = mxGetInf();	/* infinite */

  for (Idx = 0; Idx < CB.CodeWordCount; Idx++) {

      Distortion = vqDistortionTotal(CB.CodeWords + Idx, CB.CodeWordCount,
				     Vector, VEC_COMPONENT_INC,
				     CB.Weights, VEC_COMPONENT_INC,
				     CB.VectorSize);
				     
      if (Distortion < MinDistortion) {
	MinDistortion = Distortion;
	MinCodeWord = Idx + 1;	/* Matlab 1:N instead of 0:N-1 */
      }
  }

  plhs[DistortionPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
  *(mxGetPr(plhs[DistortionPos])) = MinDistortion;

  if (nlhs > 1) {
    plhs[CodewordIndexPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
    *(mxGetPr(plhs[CodewordIndexPos])) = (double) MinCodeWord;
  }
}
