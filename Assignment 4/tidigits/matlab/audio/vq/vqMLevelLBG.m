function [CodeBook, Indices] = vqMLevelLBG(CodeBook, M, Data, varargin)
% [CodeBook Indices] = vqMLevelLBG(CodeBook, M, Data, ParameterList)
%
% Builds an M level Linde Buzo Gray (LBG) VQ CodeBook for the given data set
% based upon M level splitting.  Begininning with K=1, a K word LBG code
% book is constructed and split into a 2K code book.  The 2K code book is
% then trained using the LBG algorithm.  The process continues until M such
% levels have been constructed.
%
% Input:
% M - 2^M code words.
% Data - row vector training data.
% ParameterList - As specified in vqNWordLBG
% 
% Output:
% CodeBook - An M level code book
% Indices - A vector indicating the code word to which each of the
%	training vectors would be quantized.
%
% This code is copyrighted 1997-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,inf,nargin));

Verbosity = 0;	% defaults
n=1;
while n <= length(varargin)
  switch varargin{n}
   case 'Verbosity'
    Verbosity = varargin{n+1};
    varargin(n:n+1) = [];  % delete - don't pass to vqLBG

   otherwise
    % don't process other optional argumnents, leave for vqLBG
    % All arguments must be keyword, value
    n=n+2;
  end
end

% Generate single word codebook
CodeBook.CodeWords = mean(Data);
Indices = ones(1, size(Data, 1));
Level = 1;

if Level <= M
  while Level <= M
    CodeBook = vqSplit(CodeBook, Data, Indices);  
    
    % Train with 2^M codewords.
    [CodeBook, Indices, Distortion] = vqLBG(CodeBook, Data, varargin{:});
    
    switch Verbosity
     case 2
      % Final distortion for this level
      fprintf('Level %d (%d word) distortion %f\n', Level, ...
	      size(CodeBook.CodeWords, 1), Distortion(end));
     case 3
      % Distortion at each iteration
      fprintf('Level %d (%d word) distortion/iteration ', Level, ...
	      size(CodeBook.CodeWords, 1));
      fprintf(' %f', Distortion);
      fprintf('\n');

     case 4
      % Show variance for each data set.
      for cw = 1:2^Level
	cwIndices = find(Indices == cw);
	cwN(cw) = length(cwIndices);
	cwVariance = var(Data(cwIndices,:));
	fprintf('code word %d = %d exemplars, variance:\n', cw, cwN(cw));
	cwVariance
      end
    end

    Level = Level + 1;
    
    
  end
  
  if Verbosity == 1
    fprintf('Level %d (%d word) distortion %f\n', M, ...
	    size(CodeBook.CodeWords, 1), Distortion(end));
  end
  
else
  % Degenerate case, all vectors quantize to this one codeword.
  Indices = ones(size(Data,1),1);
  % Only compute distortion if user asked for it.
  if Verbosity
    Distortion = vqDistortion(CodeBook, Data);
    fprintf('Avg Distortion for 1 word codebook:  %f\n', Distortion(end));
  end
end

CodeBook.N = 2 ^ M;

