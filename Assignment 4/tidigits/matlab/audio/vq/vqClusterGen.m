function [Data, Indices] = vqClusterGen(ClustersN, varargin)
% clustergen
%	 Data = function vqClusterGen(ClustersN, OptArgs)
%
% Generate a set of data from ClustersN clusters.  Each row
% is a single point.
%
% Optional keyword arguments
%	'Points', N	Number of points N to generate (default 1000)
%	'Dim', N	Number of dimensions N for each point (default 3)
%
% Sample Usage:
%	% generate 1000 points across 8 clusters
%	Points = clustergen(8);
%
%	generate 50 points across 2 clusters
%	Points = clustergen(2, 'Points', 50);
%
%	generate 50 2-dimensional points across 3 clusters
%	Points = clustergen(2, 'Points', 50, 'Dim', 2);
%
%	Note that since these are keyword arguments, the optional
%	arguments are position independent.
%
%	Points = clustergen(2, 'Dim', 2, 'Points', 50);

% Set up defaults
N = 1000;	% Number of points
Dim = 3;	% Number of dimensions for each point

% Check if user arguments okay
% nargchk(Min, Max, nargin) checks to see if the number of arguments
% passed in is between Min and Max.  
%
% There must be at least one 1 argument.  All other arguments
% are optional arguments which will be handled later, so we put
% the bound between 1 and infinity.  
%
% error generates an exception when passed a non-empty string.
error(nargchk(1,inf,nargin));

% handle optional keyword arguments
% When varargin appears as an argument to a function (it must be the last
% one), a cell array is created with each argument being an element of the
% cell array.  Thus varargin{1} is the first element, varargin{2} the second
% etc.  We can exploit this to easily create a keyword argument scheme.
m=1;
while m < length(varargin)
  switch varargin{m}	% Examine the mth argument
    
   case 'Dim'
    % Assume that the next argument contains the number of points
    Dim = varargin{m+1};
    m = m+2;
    
   case 'Points'
    N = varargin{m+1};
    m = m+2;
    
   otherwise
    error(sprintf('Bad optional argument: "%s"', varargin{m}));

  end
end


  MixtureSpread = 1000;	% Spread between distributions
  DataSpread=.1*MixtureSpread;	% Spread around each distribution

  % Pick a random set of means
  Means = (rand(ClustersN,Dim) - 0.5) * MixtureSpread;	

  % Pick at random to which distribution (1:ClustersN) each of the
  % N random points will belong.
  Indices = floor(rand(N,1) * ClustersN)+1; 

  % compute data.  Uniform spread about the each mean. 
  Data = randn(N, Dim)*DataSpread + Means(Indices,:);



