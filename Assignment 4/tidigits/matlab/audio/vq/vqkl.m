function [CodeBook, MinIndices] = vqkl(CodeBook, N, TrainingData, varargin)
% [CodeBook, Indices] = vqkl(CodeBook, N, TrainingData, ParameterList)
%
% This routine should not be called directly.  Use vqCreateModel.
%
% Given a skeleton codebook with weighting information, construct an N word
% codebook for the given data set using weights based upon the
% symmetric Kullback-Leibler distance
%
% ParameterList:
%
%	'Variances',  - VarianceRowVectors
%		The TrainingData will be treated as means of normal
%		distributions.  It is assumed that the components of the
%		are independent.  As a consequence of this, the variances 
%		can be organized as a matrix where the k'th row of 
%		VarianceRowVectors 
%	'Initialization', 'lbg'|'uniform'
%		Indicates how the initial codebook will be formed:
%		lbg - The Linde Buzo Grey algorithm
%		uniform (default) - Initial vectors are drawn from  
%			TrainingData assuming a unfiform distribution.
%	'Verbosity', N - Controls the degree of information printed
%		about the codebook:
%		0 - Quiet (default)
%		1 - Final distortion of N word code book
%		3 - Distortion at each iteration of codebook generation.
%		For any level above 0, the number of training vectors
%		assigned to each codeword at the end of the computation
%		is printed.
%	'Threshold', N - iteration should stop when the improvement
%		ratio falls beneath this threshold [0,1] which indicates
%		the relative improvement over the last iteration.
%
% This code is copyrighted 2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 


CodeBook.Distortion = 'kl';
CodeBook.Initialization = 'minimax';
CodeBook.N = N;

[DataN, Components] = size(TrainingData);

Verbosity = 0;	% defaults
Variances = [];
Threshold = .10;
MaxIterations = 15;

n=1;
while n <= length(varargin)
  switch varargin{n}
   case 'Verbosity'
    Verbosity = varargin{n+1}; n=n+2;

   case 'Initialization'
    CodeBook.Initialization = varargin{n+1}; n=n+2;
    
   case 'Variances'
    Variances = varargin{n+1}; n=n+2;
    
   case 'Threshold'
    Threshold = varargin{n+1}; n=n+2;
    
   otherwise
    error(sprintf('Unsupported option %s', varargin{n}));
  end
end

% error checking
if isempty(Variances)
  error('Variances keyword argument is mandatory')
else
  if size(Variances) ~= size(TrainingData)
    error('Mismatch between TrainingData (means) and Variances')
  end
end
  
switch CodeBook.Initialization
 case 'lbg'
  % generate an N word LBG codebook as starting point
  [TmpCodeBook Indices] = vqNWordLBG(CodeBook, N, TrainingData, 'Verbosity', 2);
  CodeBook.CodeWords = TmpCodeBook.CodeWords;
  % Use codewords as means, determine variances
  for cw=1:CodeBook.N
    Closest = find(Indices == cw);
    if isempty(Closest)
      error(['Too large a codebook, there are codewords that do not map' ...
	     ' to any vectors - unable to estimate variances']);
    else
      CodeBook.Variances(cw,:) = var(TrainingData(Closest,:));
    end
  end
  
 case 'minimax'
  % Use minimax strategy to choose initial clusters
  [CodeBook.CodeWords, CodeBook.Variances ] = ...
      vqklMiniMax(N, TrainingData, Variances)

 case 'uniform'
  % Select initial code words at random based upon a uniform distribution
  Indices = vqRandomIndices(N, DataN);
  CodeBook.CodeWords = TrainingData(Indices, :);
  CodeBook.Variances = Variances(Indices, :);
  
 otherwise
  error(sprintf('Bad Initialization metric "%s"', CodeBook.Initialization));
end

Distortion = zeros(150, 1);	% preallocate distortion history for speed
Iteration = 1;
figure('Name', sprintf('iteration %d', Iteration))
plot(TrainingData(:,1), TrainingData(:,2), 'k.', ...
     CodeBook.CodeWords(:,1), CodeBook.CodeWords(:,2), 'r*');

% find distortion against closest codewords
[Distortions MinIndices] = ...
    vqklDistortion(CodeBook, TrainingData, Variances);
PrevDistortion = Inf;
% note average distortion.
Distortion(Iteration) = mean(Distortions);
Improvement = 1 - (Distortion(Iteration) / PrevDistortion);

MinDistortionIteration = 1;
MinDistortion = Distortion(MinDistortionIteration);

while Improvement > Threshold & Iteration < MaxIterations
  % Determine # vectors mapping to each centroid
  ClusterSize = histc(MinIndices, 1:N);
  
  DisplayProgress(Verbosity > 2, Iteration, ClusterSize, ...
		Distortion(Iteration), Improvement);

  Iteration = Iteration + 1;
  
  % Update centroids
  ClustersToUpdate = find(ClusterSize > 0);
  for cw = ClustersToUpdate
    Mu = CodeBook.CodeWords(cw,:);
    Sigma = CodeBook.Variances(cw, :);

    % Think about the fact that the training data will contain
    % exact matches for some of the codewords when we pick them
    % as inits.  Probably okay, but need to make sure that counts
    % won't be going up continually. 

    Extend = ones(1, ClusterSize(cw));	% for Tony's trick
    ClusterIndices = find(MinIndices == cw);
    SigmaInvSum = 1 ./ Sigma(Extend,:) + 1 ./ Variances(ClusterIndices,:);
    MuNew = sum(SigmaInvSum .* TrainingData(ClusterIndices,:)) ./ ...
	    sum(SigmaInvSum);
    SigmaNew = sqrt(sum(Variances(ClusterIndices,:) + ...
			(MuNew(Extend,:) - TrainingData(ClusterIndices,:)) .^ 2) ./ ...
		    sum(1./Variances(ClusterIndices, :)));
    
    if ~ isempty(find(SigmaNew < 0))
      fprintf('Warning:  bad variance!  Entering debug');
      keyboard
    end

    CodeBook.CodeWords(cw,:) = MuNew;
    CodeBook.Variances(cw,:) = SigmaNew;
  end
  
  % find distortion against each codeword & average distortion
  [Distortions, MinIndices] = ...
      vqklDistortion(CodeBook, TrainingData, Variances);
  Distortion(Iteration) = mean(Distortions);
  
  if Distortion(Iteration)
    Improvement = 1 - Distortion(Iteration)/Distortion(Iteration-1);
  else
    Improvement = 0;	% No distortion, cannot have further improvements
  end
  figure('Name', sprintf('iteration %d - improvement %f', Iteration, Improvement))
  plot(TrainingData(:,1), TrainingData(:,2), 'k.', ...
       CodeBook.CodeWords(:,1), CodeBook.CodeWords(:,2), 'r*');
  
end

ClusterSize = histc(MinIndices, 1:N)';
DisplayProgress(Verbosity > 0, Iteration, ClusterSize, ...
		Distortion(Iteration), Improvement);

% Store history
Codebook.History = Distortion(1:Iteration);

function DisplayProgress(True, Iteration, ClusterSize, Distortion, Improvement)
if True
  fprintf('Codeword occupancy iteration %d:\n\t', Iteration - 1);
  fprintf('%d ', ClusterSize);
  fprintf('\n')
  fprintf('Iteration %d  distortion %f improvement %f\n', ...
	  Iteration, Distortion , Improvement);
end
