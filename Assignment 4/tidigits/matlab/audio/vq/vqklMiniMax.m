function [ClusterMu, ClusterVar] = ...
    vqklMiniMax(ClusterCount, Means, Variances)
% [ClusterMu, ClusterVar] = vqklMiniMax(ClusterCount, Means, Variances)
% Create ClusterCount Gaussian clusters using minimax strategy
%
% Algorithm described in:
%  @Article{shinoda2001:structural_bayes,
%    status =	 {j},
%    author = 	 {Shinoda, Koichi and Lee, Chin-Hui},
%    title = 	 {A Structural Bayes Approach to Speaker Adaptation},
%    journal = 	 {IEEE Transactions on Speech and Audio Processing},
%    year = 	 2001,
%    volume =	 9,
%    number =	 3,
%    pages =	 {276-287},
%    month =	 {March}
%  }

% Determine central Gaussian assuming equal contribution from each
% Guassian

[N, Components] = size(Means);

% Determine root distribution, eqns (3) & (4)
Root.N = 1;
Root.CodeWords = mean(Means);
Root.Variances = sum(Variances) + ...
    sum(Means .* Means) - N * (Root.CodeWords .^ 2);

% Determine first child by finding farthest Gaussian from root
Distortions = vqklDistortion(Root, Means, Variances);
[FarthestDist, FarthestIdx] = max(Distortions);

Children.N = 1;
Children.CodeWords = Means(FarthestIdx,:);
Children.Variances = Variances(FarthestIdx, :);

% MinDistortions & MinIndices contain the minimum distortion
% of each Gaussian and the associated codeword.  They are updated
% as we add each codeword.  We start with them at Inf so that the
% first codeword is not a special case.
MinDistortions = Inf;
MinDistortions = MinDistortions(ones(N,1));	% Tony's trick
MinIndices = zeros(N, 1);	% preallocate

Child = 2;
while Child <= ClusterCount
  
  fprintf('%d ', Child);
  % update distances ----------------------------------------

  LastChild = Child - 1;
  % Check distortion against last added codeword
  Distortions = vqklDistortion(Children, Means, Variances, ...
			       'Codewords', LastChild);

  % Update the Gaussians that are closest to the last added codeword
  ChangedIndices = find(Distortions < MinDistortions);
  MinDistortions(ChangedIndices) = Distortions(ChangedIndices);
  MinIndices(ChangedIndices) = ...
      LastChild(ones(length(ChangedIndices), 1));
    
  
  % add new codeword ----------------------------------------

  % Find farthest codeword 
  [FarthestDist, FarthestIdx] = max(MinDistortions);
  
  % Add it
  Children.N = Child;
  Children.CodeWords(Child, :) = Means(FarthestIdx, :);
  Children.Variances(Child, :) = Variances(FarthestIdx, :);
  
  Child = Child + 1;
end

ClusterMu = Children.CodeWords;
ClusterVar = Children.Variances;


