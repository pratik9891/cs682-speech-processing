function Poggi = vqPoggiInit(CodeBook)
% Poggi = PoggiCodeBook(CodeBook)
% Given an existing vector quantization (VQ) code book,
% perform the preprocessing for Poggi's fast VQ encoder.  
%
% This code is copyrighted 2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

% Details for Poggi's full-sarch VQ encoder may be found in:
% @Article{poggi93,
%   author = 	 {Poggi, G.},
%   title = 	 {Fast Algorithm For Full-Search VQ Encoding},
%   journal = 	 {Electronics Letters},
%   year = 	 1993,
%   volume =	 29,
%   number =	 12,
%   pages =	 {1141-1142},
%   month =	 {June},
% }
%

error(nargchk(1, 2, nargin));

Poggi = CodeBook;	% Copy attributes then modify
Poggi.type = 'poggi';

% Determine means and order them
% This information will be used for fast full-search
if Poggi.Weighted
  % Store the Weights vector as well as the weighted means
  [Poggi.Means Permutation] = ...
      sort(mean(CodeBook.CodeWords .* repmat(Poggi.Weights, Poggi.N, 1), 2));
else
  % Store the sorted means vector
  [Poggi.Means Permutation] = sort(mean(CodeBook.CodeWords, 2));
end

% Codewords ordered by mean
Poggi.CodeWords = CodeBook.CodeWords(Permutation, :);
