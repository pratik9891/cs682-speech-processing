function [CodeBook, Indices, Distortion] = vqLBG(CodeBook, Data, varargin)
% [Codewords, Indices, Distortion] = ...
%	vqLBG(CodeBook, Data, ParameterList)
%
% Builds a Linde Buzo Gray VQ Codebook for the given data set.
% CodeBook is the starting codebook containing row vector code words.
% Data contains row vector training data.
%
% (Name should really be changed, we used to do splitting here (LBG),
%  this is just the K-Means algorithm here...)
%
% ParameterList may be:
%
%	Threshold, N - LBG iteration should stop when the improvement
%		ratio falls beneath this threshold.  Default .01.
%
% Output:
%	Codewords - Collection of minimum distortion code words.
%		Each code word is a row vector.
%	Indices - Index of minimun distortion codeword for each
%		training vector Data(n,:).
%	Distortion - Average distortions of training data for each
%		iteration.
%
% This code is copyrighted 1997-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2, inf, nargin));

DistortionThreshold = .01;	% default
Verbosity = 0;

n=1;
while n <= length(varargin)
  switch varargin{n}
   case 'Threshold'
    DistortionThreshold = varargin{n+1}; n=n+2; 
    
   case 'Verbosity'
    Verbosity = varargin{n+1}; n=n+2;	% keyword supported, no-op
    
   otherwise
    error(sprintf('Bad optional argument: "%s"', varargin{n}));
  end
end

Distortion = zeros(10,1);	% preallocate distortion history for speed

% Determine original distortion
Iteration = 1;
[Distortion(Iteration), Indices] = vqDistortion(CodeBook, Data);

Improvement = Inf;
while Improvement > DistortionThreshold
  Iteration = Iteration + 1;
  CodeBook.CodeWords = vqCentroids(CodeBook, Data, Indices);
  
  [Distortion(Iteration), Indices] = vqDistortion(CodeBook, Data);
  if Distortion(Iteration)
    Improvement = (Distortion(Iteration-1) - Distortion(Iteration)) ...
	/ Distortion(Iteration);
  else
    Improvement = 0;	% No distortion, cannot have further improvements
  end
end

% Remove unused distortion history
if length(Distortion) > Iteration
  Distortion(Iteration+1:end) = [];
end
