function [Codebook, Data] = vqtest(Codewords, varargin)
% vqtest [Codebook, Data] = function vqtest(Codewords, OptArgs)
%
% Demonstration of vector quantization.  Generates a Codewords vector
% quantizer based upon synthetic data which is either provided (via the
% optional keyword argument 'Data' and data matrix) or
% generated automatically. 
%
% OptArgs are optional keyword arguments:
%	'Data', DataSet - each row is a training vector
%	'Clusters', Number of distributions from which to draw 
%		samples from.
%	'Points', Number of samples to generate.
%	'Demo', false|true - Plot the results of each binary split.
%	Other arguments will be passed to vqCreateModel
%
% Example:
%	vqtest(64, 'Clusters', 256, 'Points', 10000, 'Demo', 1)
%
%
% This code is copyrighted 2002 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

n = 1000;	% Number of points
Clusters = 7;	% Number of distributions
Data = [];
Demo = 0;
Dim = 3;	% dimensionality of space

% handle arguments which are not passed to the codebook
% creation, deleting them as they are processed.
m=1;
while m < length(varargin)
  switch varargin{m}
   case 'Data'
    % Process the optional argument
    Data = varargin{m+1};
    % Delete optional args from the varargin list as 
    % the list will be passed to other functions.
    varargin(m:m+1) = [];
    
   case 'Clusters'
    Clusters = varargin{m+1};
    varargin(m:m+1) = [];  % Remove so not passed to clustering algorithm
    
   case 'Demo',
    Demo =  varargin{m+1};
    varargin(m:m+1) = [];

   case 'Dim'
    Dim = varargin{m+1};
    varargin(m:m+1) = [];
    
   case 'Points'
    N = varargin{m+1};
    varargin(m:m+1) = [];
    
   otherwise
    m=m+2;

  end
end


if isempty(Data)
  % Generate data if user did not provide
  [Data, DataClass] = vqClusterGen(Clusters, 'Points', n, 'Dim', Dim);
  % Generate ugly different colors, get rid of white
  % assuming that we are plotting to a white background.
  classColors = hsv(Clusters+1);
  classColors(sum(classColors, 2) == 3, :) = [];  % remove white row
else
    Indices = ones(size(Data, 1), 1);
end

Codebook = vqCreateModel(Codewords, Data, 'Verbosity', 1, varargin{:});
figure('Name', sprintf('%d word codebook', Codewords));
title(sprintf('%d word codebook', Codewords))
hold on;
for cidx = 1:Clusters
    vqPlotBook(Data(DataClass == cidx, :), '.', 'Color', classColors(cidx,:));
    hold on;
end
vqPlotBook(Codebook.CodeWords, 'gh', 'MarkerSize', 16, 'MarkerFaceColor', 'r')

if Demo
  % Plot the power of two codebooks leading up to the one we just
  % created.  This is duplicate work, but the vqCreateModel doesn't
  % maintain a history of the splits.
  % ceil(log2(.) - floor(log2(.))) subtracts one when the number of 
  % Codewords is not a power of two as the binary split algorithm
  % only creates power of two codebooks.
  Levels = floor(log2(Codewords)) + ...
	   ceil(log2(Codewords) - floor(log2(Codewords)));
  Level = 0;
  while Level < Levels
    figure('Name', sprintf('Level %d', Level))
    cb = vqCreateModel(2^Level, Data, varargin{:});
    for cidx = 1:Clusters
      vqPlotBook(Data(DataClass == cidx, :), '.', 'Color', classColors(cidx,:));
      hold on;
    end
    vqPlotBook(cb.CodeWords, 'gh', 'MarkerSize', 16, 'MarkerFaceColor', 'r');
    title(sprintf('%d word codebook', 2^Level))
    hold off
    Level = Level + 1;
  end
end

