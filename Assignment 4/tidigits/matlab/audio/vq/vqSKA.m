function [MinCodeBook, MinClusterIndices, MinDistortion] = ...
    vqSKA(CodeBook, N, Data, varargin)
% function [CodeBook, Indices, Distortion] = vqSKA(CodeBook, N, Data, OptArgs)
%
% Builds a codebook using the Stochastic Segemental K-Means Algorithm.
%
% "Stochastic K-means algorithm for vector quantization"
% K�vesi, Balazs and Boucher, Jean-Marc and Saoudi, Samir
% _Pattern Recognition Letters_, V 22, No 6-7, pp 603-610, May 2001
%
%
% Optional arguments:
%	'Verbosity'
%
% This code is copyrighted 2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

[DataN, Components] = size(Data);

Verbosity = 0;	% defaults

n=1;
while n <= length(varargin)
  switch varargin{n}
   case 'Verbosity'
    Verbosity = varargin{n+1};
    n=n+2;

   otherwise
    error(sprintf('Unsupported option %s', varargin{n}));
  end
end

CodeBook.Initialization = 'uniform';
switch CodeBook.Initialization
 case 'lbg'
  CodeBook = vqNWordLBG(CodeBook, N, Data, 'Verbosity', 2);
  
 case 'uniform'
  CodeBook.N = N;
  CodeBook.CodeWords = zeros(N, Components);
  
  % Pick initial centers based upon uniform distribution
  ClusterIndices = randperm(DataN);
  ClusterIndices = ClusterIndices(1:CodeBook.N);
  CodeBook.CodeWords = Data(ClusterIndices,:);
end

% Stopping condition.  N iterations without improvement.
MaxIterationsWithoutNewMin = 20;

Distortion = zeros(150, 1);	% preallocate distortion history for speed

Iteration = 1;

% find distortion against each codeword & average distortion
Distortions = vqFullDistortion(CodeBook, Data);
Distortion(Iteration) = sum(min(Distortions, [], 2)) / DataN; 

MinDistortionIteration = 1;
MinDistortion = Distortion(MinDistortionIteration);
MinCodeBook = CodeBook;

Iteration = Iteration + 1;
while Iteration - MinDistortionIteration <= MaxIterationsWithoutNewMin
  
  % build the discrete pdf and probabilistically assign to new clusters
  Pdf = vqSKApdf(BetaSKA(Iteration), Distortions);
  ClusterIndices = stDiscreteDraw(Pdf);
  
  CodeBook.CodeWords = vqCentroids(CodeBook, Data, ClusterIndices);
  
  % find distortion against each codeword & average distortion
  Distortions = vqFullDistortion(CodeBook, Data);
  Distortion(Iteration) = sum(min(Distortions, [], 2)) / DataN; 
  
  if Distortion(Iteration) < MinDistortion
    % new local min - note it
    MinCodeBook = CodeBook;
    MinDistortionIteration = Iteration;
    MinDistortion = Distortion(Iteration);
  end
  
  if Verbosity > 1
    fprintf('Codeword occupancy iteration %d:\n\t', Iteration - 1);
    fprintf('%d ', histc(ClusterIndices, 1:CodeBook.N));
    fprintf('\n')
    fprintf('Iteration %d (%d since last min %f), distortion %f\n', ...
	    Iteration, Iteration - MinDistortionIteration, ...
	    MinDistortion, Distortion(Iteration));
  end

  Iteration = Iteration + 1;
end

% Obtain the indices from the minimum distortion codebook.
if nargout > 1
  [MinDistortion, MinClusterIndices] = vqDistortion(MinCodeBook, Data);
end

%------------------------------------------------------------
function Result = BetaSKA(n)

Result = 2 * log(n);
