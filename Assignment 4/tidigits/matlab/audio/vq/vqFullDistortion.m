% function [Distortions Indices] = 
%	vqFullDistortion(Codebook, Vectors, Optional Arguments)
%
% Given a codebook, and a set of row vectors, compute the 
% distortion against each codeword of the codebook.
%
% It is assumed that Vectors is a matrix of N row vectors.
%
% Optional arguments:
%
%	'Weights', WeightMatrix - Matrix of the same dimension as
%		Vectors.  Each row vector of Weights will be used
%		to weight the distortion between the corresponding
%		vector and the code words.  These weights supercede
%		any weighting associated with the Codebook itself.
%	'Codeword', N - Only determine the distortion for codeword index
%		N.  When 'Codeword' is specified, the value of Indices
%		is undefined.
%
% The Distortions will be returned in a N x Codewords size matrix (unless
% optional argument 'Codeword' is present in which case Distortions will
% be N x 1).
%
% If the optional output argument Indices is present, a vector of minimum
% code word distortion codeword indices is returned as well.
%
% This code is copyrighted 2001-2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 



