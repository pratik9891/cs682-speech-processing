#include <mex.h>
#include <matrix.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "lib/vqlib.h"

/* Optimization of variance reestimation for independent components */

#define MAXSTRING	255

/* gateway function
 *
 * LHS Arguments:
 *	Distortions
 *	CodewordIndices
 * RHS Arguments:
 *	Codebook
 *      Vectors
 *	Weights (optional)
 *
 * Given a vector quantizer CodeBook, and a matrix of row Vectors,
 * find the distortion of each Vector against the code book.
 *
 * An optional Weight matrix (of the same size as Vectors) permits
 * one to override any Weighting specified by the CodeBook on a
 * per Vector basis.
 *
 * Distortion is a column vector of Euclidean distortion measures.  If
 * a component weighting is present in the CodeBook, it will be used
 * in the distortion computation.
 *
 * This code is copyrighted 1999-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 * */


void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray 
	    *prhs[])
{
  /* positional parameters */
  const int	CodeBookPos = 0;
  const int	VectorPos = 1;
  const int	InputArgMinCount = 2;
  
  const int	DistortionPos = 0;
  const int	CodeWordIndexPos = 1;

  double const	*Vectors, *Weights;
  double const	*VectorPtr;
  CodeBook	CB;

  double	Distortion, MinDistortion;
  double	*MinCodeWordPtr, *DistortionBase, *DistortionPtr;
  int		MinCodeWord;
  int		VectorCount;
  int		CodeWordIdx, VectorIdx;
  int		OffsetToNextWeightComponent;
  int		i;
  int		CodeWordMin, CodeWordMax;
  char		String[MAXSTRING];
  char		Message[MAXSTRING];

  typedef enum {
    WT_Static,
    WT_Dynamic,
    WT_None
  } WeightType;

  WeightType Weighted;
    
  /* check args */
  if (nrhs < InputArgMinCount)
    mexErrMsgTxt("Bad argument count");

  /* Retrieve CodeBook */
  vqCodeBookAccessor(prhs[CodeBookPos], &CB);

  /* Retrieve vectors */
  Vectors = mxGetPr(prhs[VectorPos]);
  VectorCount = mxGetM(prhs[VectorPos]);

  /* check vector is of same dimensionality as codewords. */
  if (CB.VectorSize != (mxGetN(prhs[VectorPos]))) {
    
    sprintf(Message, "Codebook/Vector size mismatch:  "
	    "Codebook: %d x %d, Vector %d x %d",
	    CB.CodeWordCount, CB.VectorSize,
	    mxGetM(prhs[VectorPos]),
	    mxGetN(prhs[VectorPos]));
    mexErrMsgTxt(Message);
  }
  
  /* Set up defaults which may be overriden --------------- */
  CodeWordMin = 0;
  CodeWordMax = CB.CodeWordCount - 1;

  if (CB.Weights) {
    /* Static weighting scheme present in codebook */
    Weighted = WT_Static;
    Weights = CB.Weights;
    OffsetToNextWeightComponent = COL_NEXTCOL_OFFSET(1, Components);
  } else {
    /* no weighting */
    Weighted = WT_None;
    Weights = NULL;
    OffsetToNextWeightComponent = 0;
  }
  
    
  /* Retrieve optional arguments --------------- */
  i = InputArgMinCount;
  while (i < nrhs) {
    if (! mxIsChar(prhs[i])) {
      mexErrMsgTxt("Optional arguments must start with a keyword");
    }
    mxGetString(prhs[i], String, MAXSTRING - 1);
    if (! strcmp(String, "Weights")) {
      Weighted = WT_Dynamic;

      /* Verify Weights matrix is of the same size as the Vectors */
      if (! mxIsDouble(prhs[i+1]) ||
	  CB.VectorSize  != mxGetN(prhs[i+1]) ||
	  mxGetM(prhs[VectorPos]) != mxGetM(prhs[i+1])) {
	
	sprintf(Message, "Vectors/Weights size or type mismatch:  "
		"Vectors: %d x %d, Weights %d x %d",
		mxGetM(prhs[VectorPos]), CB.VectorSize,
		mxGetM(prhs[i+1]), mxGetN(prhs[i+1]));
	mexErrMsgTxt(Message);
      }

      Weights = mxGetPr(prhs[i+1]);
      i = i + 2;

    } else if (! strcmp(String, "Codeword")) {
      if (! mxIsNumeric(prhs[i+1]))
	mexErrMsgTxt("Codeword argument must be numeric");
      if (mxGetM(prhs[i+1]) * mxGetN(prhs[i+1]) != 1)
	mexErrMsgTxt("Codeword argument must be a scalar");

      /* Set range to codeword user specified & convert to C indexing */
      CodeWordMin =  (int) mxGetScalar(prhs[i+1]) - 1;
      if (CodeWordMin < 0 || CodeWordMin > CodeWordMax) {
	sprintf(Message, "Codeword argument must be in [1, %d]", 
		CB.CodeWordCount);
	mexErrMsgTxt(Message);
      }
      CodeWordMax = CodeWordMin;	/* only process one */
      i = i + 2;

    } else {
      sprintf(Message,
	      "Optional argument '%s' not a valid parameter keyword", String);
      mexErrMsgTxt(Message);
    }
  }

  /* allocate output args */
  plhs[DistortionPos] =
    mxCreateDoubleMatrix(VectorCount, CodeWordMax - CodeWordMin + 1, mxREAL);
  DistortionBase = mxGetPr(plhs[DistortionPos]);
  
  if (nlhs > CodeWordIndexPos) {
    plhs[CodeWordIndexPos] =
      mxCreateDoubleMatrix(VectorCount, 1, mxREAL);
    MinCodeWordPtr = mxGetPr(plhs[CodeWordIndexPos]);
  } else
    MinCodeWordPtr = NULL;	/* don't store min code word */
  
  /* compute distortion */

  for (VectorIdx = 0; VectorIdx < VectorCount; VectorIdx++) {

    /* locate correct row */
    VectorPtr = Vectors + VectorIdx;
    DistortionPtr = DistortionBase + VectorIdx;
    
    MinDistortion = mxGetInf();	/* infinite */

    for (CodeWordIdx = CodeWordMin; CodeWordIdx <= CodeWordMax;
	 CodeWordIdx++) {

      (*DistortionPtr) =
	Distortion =
	 vqDistortionTotal(CB.CodeWords + CodeWordIdx,
			   COL_NEXTCOL_OFFSET(CB.CodeWordCount, Components),
			   VectorPtr,
			   COL_NEXTCOL_OFFSET(VectorCount, Components),
			   Weights, OffsetToNextWeightComponent,
			   CB.VectorSize);
      
      if (Distortion < MinDistortion) {
	MinDistortion = Distortion;
	MinCodeWord = CodeWordIdx + 1;	/* Matlab 1:N instead of 0:N-1 */
      }

      /* Move output to next column */
      DistortionPtr = DistortionPtr + VectorCount;
    } /* end for codewords */

    if (Weighted == WT_Dynamic) {
      /* move to next row of weights matrix */
      Weights++;
    }

    if (MinCodeWordPtr) {
      /* User wants to know which code word is min distortion */
      *MinCodeWordPtr++ = MinCodeWord;
    }

  } /* end for vector */
}
