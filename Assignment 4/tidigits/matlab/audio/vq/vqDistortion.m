function [Distortion, CodeWordIndices] = vqDistortion(CodeBook, Vectors)
% [Distortion, CodeWordIndices] = vqDistortion(CodeBook, Vectors)
% Given a codebook, either compute the minimum distortion for one
% row vector or the average distortion for a series of vectors.
%
% Distortion is a Euclidean distortion measure.  If a component
% weighting is present in the CodeBook, it will be used in the
% distortion computation.
%
% CodeWordIndicess contains a vector of code-word indices that result
% in the minimum distortion for each Vector.
%
% This code is copyrighted 1999-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,2,nargin))

NumVectors = size(Vectors,1);

if NumVectors > 1
  CodeWordIndices = zeros(size(Vectors, 1),1);
  [Distortion, CodeWordIndices(1)] = ...
      vqPDEDistortionSV(CodeBook, Vectors(1,:));
  for n = 2:NumVectors
    [VDistortion, CodeWordIndices(n)] = ...
	vqPDEDistortionSV(CodeBook, Vectors(n,:), CodeWordIndices(n-1));
    Distortion = Distortion + VDistortion;
  end
  Distortion = Distortion / NumVectors;
else
  % single vector
  [Distortion, CodeWordIndices] = vqPDEDistortionSV(CodeBook, Vectors);
end
