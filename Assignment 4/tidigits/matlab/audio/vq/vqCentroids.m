function CodeWords = vqCentroids(CodeBook, Data, Indices)
% CodeWords = vqCentroids(CodeBook, Data, Indices)
% Reestimate the centroids of a codebook:
% 1.  Determine the minimum distortion codeword for each data vector 
% 2.  Compute centroid of associated data vectors for each codeword.
%
% The optional argument Indices specifies the codewords to which
% each of the Data vectors as are assigned.  Providing the indices
% vector avoids a call to vqDistortion.
%
% This code is copyrighted 1997-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

NumVectors = size(Data, 1);
Debug = 0;

if nargin < 3
  % Assign each observation to the min distortion codeword.
  [Distortion, Indices] = vqDistortion(CodeBook, Data);
end

% Copy existing code book (preallocates + initializes unchanged codewords)
CodeWords = CodeBook.CodeWords;

% Compute centroids
for c=1:size(CodeBook.CodeWords,1)
  ContributingIndices = find(Indices == c);
  if Debug
    fprintf('Code word %d indices: ', c); 
    ContributingIndices'
  end
  if ~ isempty(ContributingIndices)
    % find centroid of all vectors quantized to this code word 
    CodeWords(c,:) = sum(Data(ContributingIndices,:),1) / ...
	length(ContributingIndices);
  end
end
