/*
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#include "vqlib.h"

/* vqDistortionPartial - Compute the distortion between a test
 * vectors and a specific codeword.  Distortion is computed
 * incrementally, and computation will stop if at any time it
 * exceeds the maximum distortion stored in the PartialDistortionPtr
 *
 * Arguments:
 *	CodeWordPtr - points to a specific code word in the code book
 *	NextComponent - Offset to move to the next component of the code
 *		code word.
 *	VectorPtr - Vector against which distortion is to be checked.
 *	VectorSize - Number of components
 *	WeightsPtr - Pointer to an array of component weights or NULL
 *		if no component weighting is to be used.
 *	PartialDistortionPtr - Pointer to return code structure.
 */
void vqDistortionPartial(const double *CodeWordPtr,
			 int NextCodeWordComponent,
			 const double *VectorPtr,
			 int NextVectorComponent,
			 const double *WeightsPtr,
			 int NextWeightsComponent,
			 int Components,
			 PartialDistortion *ResultsPtr)
{
  int		Component;
  double	Diff, Distortion;

  ResultsPtr->Distortion = 0;
  ResultsPtr->Components = 0;
  
  if (WeightsPtr) {
    while (ResultsPtr->Components < Components &&
	   ResultsPtr->Distortion < ResultsPtr->MinDistortion) {
      Diff = (*CodeWordPtr - *VectorPtr);
      ResultsPtr-> Distortion += *WeightsPtr * Diff * Diff;
      CodeWordPtr += NextCodeWordComponent;	/* step through matrix */
      VectorPtr += NextVectorComponent;
      WeightsPtr += NextWeightsComponent;
      ResultsPtr->Components++;
    }
  } else {
    while (ResultsPtr->Components < Components &&
	   ResultsPtr->Distortion < ResultsPtr->MinDistortion) {
      Diff = *CodeWordPtr - *VectorPtr;
      ResultsPtr->Distortion += Diff * Diff;
      CodeWordPtr += NextCodeWordComponent;	/* step through matrix */
      VectorPtr += NextVectorComponent;
      ResultsPtr->Components++;
    }
  }

  return;
}
