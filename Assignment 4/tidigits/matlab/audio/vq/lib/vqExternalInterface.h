/*
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#ifndef VQEXTERNAL_INTERFACE_H
#define VQEXTERNAL_INTERFACE_H 1

#include <mex.h>
#include <matrix.h>

/* convert Matlab codebook to C codebook */
void vqCodeBookAccessor(mxArray const *MxSourceBook, CodeBook *DestBook);

/* compute the partial distortion of a vector against a single
 * codeword using partial distance elimination.  The lower bound
 * distance must be populated in the PartialDistortion structure
 */
void vqDistortionPartial(const double *CodeWordPtr,
			 int NextCodeWordComponent,
			 const double *VectorPtr,
			 int NextVectorComponent,
			 const double *WeightsPtr,
			 int NextWeightsComponent,
			 int Components,
			 PartialDistortion *ResultsPtr);

/* compute the full distortion of a vector against a single codeword */
double vqDistortionTotal(const double *CodeWordPtr,
			 int NextCodeWordComponent,
			 const double *VectorPtr,
			 int NextVectorComponent,
			 const double *WeightsPtr,
			 int NextWeightsComponent,
			 int Components);


#endif
