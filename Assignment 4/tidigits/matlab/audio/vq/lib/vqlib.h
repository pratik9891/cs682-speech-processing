/*
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#ifndef VQLIB_H
#define VQLIB_H 1

/* Unlike C, Matlab uses column major indexing - conversion macros */
#define COL_MAJOR(ROW, COL, NUMROWS)	(ROW) + (NUMROWS) * (COL)
#define COL_ACCESS(PTR, ROW, COL, NUMROWS) \
	((PTR) + COL_MAJOR((ROW), (COL), (NUMROWS)))
#define COL_INCROW(MATRIXPTR, ROWSIZE, COLSIZE)	(MATRIXPTR)++
#define COL_INCCOL(MATRIXPTR, ROWSIZE, COLSIZE)	((MATRIXPTR) += (ROWSIZE))
#define COL_NEXTCOL_OFFSET(ROWSIZE, COLSIZE) (ROWSIZE)
#define COL_NEXTROW_OFFSET(ROWSIZE, COLSIZE) 1

#define ROW_MAJOR(ROW, COL, NUMCOLS)	(ROW) * (NUMCOLS) + (COL)
#define ROW_ACCESS(PTR, ROW, COL, NUMCOLS) \
	((PTR) + ROW_MAJOR(ROW, COL, NUMCOLS))
#define ROW_INCROW(MATRIXPTR, ROWSIZE, COLSIZE)	((MATRIXPTR) += (COLSIZE))
#define ROW_INCCOL(MATRIXPTR, ROWSIZE, COLSIZE)	((MATRIXPTR)++)
#define ROW_NEXTCOL_OFFSET(ROWSIZE, COLSIZE) 1
#define ROW_NEXTROW_OFFSET(ROWSIZE, COLSIZE) (COLSIZE)

/* structure used for communicating information about partial
 * distance elimination distortions.
 */

#include "vqTypes.h"
#include "vqExternalInterface.h"

#endif
