function [Distortions, Indices] = ...
    vqBhattacharyyaDistortion(CodeBook, Means, Variances, varargin)
% function [Distortions Indices] = 
%	vqFullDistortion(Codebook, Means, Variances)
%
% Given a codebook, a set of row vectors representing the Means of an normal
% distribution with independent components, and their respective Variances
% (also row vectors), compute the distortion against each codeword of the
% codebook.
%
% The optional output Indices indicates the indices of the codewords
% which produce minimum distortion with each mean vector.
%
% Optional arguments:
%
%	'Codewords', [Indices] - Only determine the distortion for codeword
%		indices.  When 'Codewords' is specified, the value of Indices
%		is represents the minimal distortion codewords from the
%		within the set of indices.
%
% The Distortions will be returned in a N x Codewords size matrix (unless
% optional argument 'Codeword' is present in which case Distortions will
% be N x 1).
%
% If the optional output argument Indices is present, a vector of minimum
% code word distortion codeword indices is returned as well.
%
% This code is copyrighted 2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(3,inf,nargin))

n=1;
Codewords = [];
while n <= length(varargin)
  switch varargin{n}
   case 'Codewords'
    Codewords = varargin{n+1}; n=n+2;
   otherwise
    error(sprintf('Bad keyword argument "%s"', varargin{n}));
  end
end

NumVectors = size(Means,1);

if isempty(Codewords) 
  Codewords = 1:CodeBook.N;
else
  if ~ utIsVector(Codewords, 'row')
    Codewords = Codewords';
  end
end

Distortions = zeros(NumVectors, 1);

% Compute determinant of all of the "variance-covariance matrices"
DetVariances = prod(Variances, 2);
DetCodeBook = prod(CodeBook.Variances, 2);

%STOPPED HERE - CW W/ ITSELF NOT RETURNING ZERO DISTORTION
for idx=1:NumVectors
  MinDistortion = Inf;
  
  SqrtProductOfDeterminants = sqrt(DetVariances(idx) * DetCodeBook);
  MeanVariances = (Variances(idx * ones(CodeBook.N,1),:) + CodeBook.Variances) ...
      ./2;
  OffsetsSq = ...
      (Means(idx * ones(CodeBook.N,1),:) - CodeBook.CodeWords) .^ 2;
  Term1 = ...
      prod(MeanVariances .* OffsetsSq, 2) / 8;
  Term2 = log(prod(MeanVariances, 2) ./ SqrtProductOfDeterminants) / 2;
  CWDistortions = Term1 + Term2;
  [Distortions(idx), Indices(idx)] = min(CWDistortions);
  %for cw=Codewords
    % Bhattacharya bound
    % Note that we make simplifications due to the independence
    % assumption.
    % Complete formula is:
    % k(1/2) = 1/8 (u2 - u1)' [.5(Sigma_1 + Sigma_2)]^-1 (mu2 - mu1) +
    %	       1/2 ln (|.5(Sigma1 + Sigma_2)|)/ sqrt(|Sigma1| |Sigma2|)
    
    
%    MeanVariance = .5 * Variances(idx,:) + CodeBook.Variances(cw,:);
%    Distortion = 1/8 * ...
%	sum((Means(idx,:) - CodeBook.CodeWords(cw,:)) .^ 2 .* ...
%	    1 ./ MeanVariance + ...
%	    .5 * log(prod(MeanVariance)/ SqrtProductOfDeterminants(cw)));


    % If smallest so far, save information
%    if Distortion < MinDistortion
%      MinDistortion = Distortion;
%      Indices(idx) = cw;
%    end
%  end
end
