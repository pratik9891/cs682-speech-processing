function Models = TrainRecognizer(ClassLabels, TrainSet, Method, N, FeatureDir)
% Models = TrainRecognizer(BaseDir, Classes, TrainSet, CodeWords)
%       ClassLabels - Cell array with names of classes
%       TrainSet - Cell array.  Each cell contains a cell array
%               with names of training data files.
%       Method - 'vq' vector quantization, 'gmm' - Gaussian mixture model
%       N - Number of codewords or mixtures
%       FeatureDir - Root directory for features 
%


narginchk(5,5);     % need exactly N arguments

ClassesN = length(ClassLabels);  % # categories?

%Append the extension to the train data
TrainSet = cellfun(@(S) strcat(S,'_01.mfc'), TrainSet, 'Uniform', 0);

tic	% start timer
for c=1:ClassesN
    Models(c).Label = ClassLabels{c};
    fprintf('Class %s\n', Models(c).Label);    % show current class
    
    % Read pooled training data for class
    Features = [];
    % loop on TrainSet{c} building Features
    numberOfFilesPerClass = size(TrainSet{c,1},2);
    for i=1:numberOfFilesPerClass
        Features = [Features,spReadFeatureDataHTK(TrainSet{c,1}{1,i})];
    end
    
    switch Method
        case 'vq'
            % circa 1980s speech & speaker recognition at its finest...
            Models(c).Codebook = TrainKMeans(N,Features);
        case 'gmm'
            % Train a GMM, feature vectors must be rows
            Models(c).GMM = hmmCreateGMM(N,Features');             
        case 'gmm-my'
            Models(c).GMM = gmmCreate(N,Features',true);
        otherwise
            error('Unsupported modeling method %s', Method);
    end
end

fprintf('\n')
  
Elapsed_s = toc;
fprintf('Elapsed training time %f m\n', Elapsed_s/60);
