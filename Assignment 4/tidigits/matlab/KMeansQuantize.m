function [minDistMean, minDistortion, minCodeword] = ...
    KMeansQuantize(codebook, TestVectors)
% [minDistMean, minDistances, minCodeword] = ...
%       KMeansQuantize(codebook, TestVectors)
% Given a column oriented codebook and set of test vectors, return
% the mean of the minimum distortions between each test vector and
% the closest codeword, the individual minimum distortions, and the 
% indices of the closest codewords.

% Compute pair wise distortions
% distortions(i,j) is distortion between ColVecs(:,i) and Codebook(:,j)
distortions = distortion3(TestVectors, codebook);

[minDistortion, minCodeword] = min(distortions);

% For each ColVec i, min distortion codeword is one with smallest
% distortion.  Take average of smallest distortions
minDistMean = mean(minDistortion);