% Shows demonstration of vector quantization
% Plots codewords with triangles of different colors
% Plots training vectors as points in the same color as the cluster 
% to which they belong.  Coloring the clusters was the idea of one
% of my students, Jim Du.


CodeWordsN = 5; % # of codewords
TrainN = 5000;   % # of training vectors
Dim = 3;        % data dimension
Clusters = 7;   % number clusters

TrainData = vqClusterGen(Clusters, 'Points', TrainN, 'Dim', Dim)';
CodeWords = VQ_Train(TrainData, CodeWordsN);
% plot them (not part of the assignment)

% Generate regularly spaced colors.  The first and last
% are white & black, so we add 2 to the number we want.
colors = cool(CodeWordsN+2);   
figure('Name', 'partition of data')

% Recompute distortions so that we can color each training
% vector by final partition.
Distortions = distortion3(TrainData, CodeWords);
[mindist, MinCodeword] = min(Distortions);

for k=1:CodeWordsN
    % Find vectors associated with each codeword
    Partition_k = find(MinCodeword == k);
    % plot points in unique color
    plot3(TrainData(1, Partition_k), TrainData(2, Partition_k), ...
        TrainData(3, Partition_k), '.', 'Color', colors(k+1, :));
    hold on
    plot3(CodeWords(1,k), CodeWords(2,k), CodeWords(3,k), '^', ...
        'Color', colors(k+1,:))
end
xlabel('x')
ylabel('y')
zlabel('z')
title('Codeword vectors (\Delta) and partition of the training set (.)')
