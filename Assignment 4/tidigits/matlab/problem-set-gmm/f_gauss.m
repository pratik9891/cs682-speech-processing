function P = f_gauss(RowVectors, mu, Sigma)
% P = f_gauss(RowVectors, mu, Sigma)
%
% Returns the likelihood for each vector in a set of RowVectors given a
% normal distribution (Gaussian) with mean mu as a row vector and
% variance-covariance matrix Sigma.  P is a column vector where
%
% P(i) = f_gauss(RowVectors(i, :), mu, Sigma)
 


narginchk(3,3);  % Check input arguments

% N observations of size Dim:
%  x1(1) x1(2) ... x1(Dim)
%  x2(1) x2(2) ... x2(Dim)
%  ...
%  xN(1) xN(2) ... xN(Dim)
[N, Dim] = size(RowVectors);

% Determine offsets from mean.
% We'll use Tony's trick to expand mu into a matrix we can subtract.
MeanOffset = RowVectors - mu(ones(N,1), :);

% Check if this is a diagonal matrix.  If so, we can do things much
% faster.  
diagonal = sum(sum(Sigma)) - sum(diag(Sigma)) < 1e-10;

if diagonal
    diagSigma = diag(Sigma)';
    detSigma = prod(diagSigma);  % determinant
    SigmaInv = 1./diagSigma;
    ExpArg = zeros(N, 1);
    %for idx=1:N
        % special case - diagonal covariance matrix
    %    ExpArg(idx) = sum(MeanOffset(idx,:).^2 .* SigmaInv);
    %end
    % vectorize it!! as suggested by scott! Super cool
    MeanOffset = MeanOffset.^2;
    ExpArg = sum(bsxfun(@times,MeanOffset,SigmaInv),2);
    
else    
    % Constants we would prefer not to compute multiple times if
    % this were a production implementation ...
    SigmaInv = inv(Sigma);
    detSigma = det(Sigma);  % determinant

    % The only part of this operation that we cannot express as a 
    % single matrix operation is the computation of the the 
    % argument to the exponential.  We will loop through that
    % part and compute the rest later.

    ExpArg = zeros(N, 1);
    for idx=1:N
      ExpArg(idx) = MeanOffset(idx,:) * SigmaInv * MeanOffset(idx,:)';
    end
end

% Again, in a production version we would recompute this value
% that does not change with respect to the data whose likelihoods
% are being computed.
k = 1 ./ ((2 * pi)^(Dim/2) * sqrt(detSigma));

P = k * exp(-.5 .* ExpArg);

