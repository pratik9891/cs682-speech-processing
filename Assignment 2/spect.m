[x,Fs] = wavread('2-imp.wav');
figure;
subplot(2,1,1);
spectrogram(x, hamming(0.005*Fs), 'yaxis');
title('Narrow-Band window length(5ms)');
subplot(2,1,2);
spectrogram(x, hamming(0.02*Fs), 'yaxis');
title('Wide-Band window length(20ms)');