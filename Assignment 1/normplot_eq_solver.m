function [threshold1,threshold2] = normplot_eq_solver(mu_n, var_n, mu_s, var_s)
% Takes as arguments means and variances of both the equations and gives
% returns the intersection point

A = (var_n - var_s);

B = 2*((var_s*mu_n)-(var_n*mu_s));

C = (var_n*mu_s*mu_s)-(var_s*mu_n*mu_n)-(2*var_s*var_n)*(log(sqrt(var_n)/sqrt(var_s)));

threshold1 = (-B+sqrt((B*B)-(4*A*C)))/(2*A);
threshold2 = (-B-sqrt((B*B)-(4*A*C)))/(2*A);
